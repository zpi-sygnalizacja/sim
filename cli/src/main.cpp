// cli
#include "cli/create_process.hpp"

// persistence
#include <persistence/Data_source.hpp>

// logic
#include <logic/Light_algorithm_executor.hpp>
#include <logic/Simulator.hpp>

// util
#include <util/timed_invoke.hpp>

// str
#include <str/chrono_to_str.hpp>
#include <str/format.hpp>

// SWS
#ifdef ENABLE_REST
	#include <cli/server_http.hpp>
	#include <cli/client_http.hpp>
#endif

// std
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

inline void run_simulation(
	std::istream& is,
	const std::string& output_path,
	std::chrono::minutes simulation_duration,
	const std::string& analyze_path)
{
	persistence::Data_source source {is};

	decltype (source.load()) intersection;

	std::ofstream output_file {output_path, std::ofstream::trunc};
	auto os = output_path.empty() ? &std::cerr : &output_file;

	try
	{
		intersection = source.load();
	}
	catch (std::exception& e)
	{
		std::cout << "Error while loading the intersection\n"
			<< "\tline no: " << source.get_line_no() <<  '\n'
			<< "\twhat: " << e.what() << '\n';
		return;
	}

	std::cout << "# Loaded intersection\n"
		<< intersection.first
		<< "\n# The algorithm:\n"
		<< intersection.second
		<< "\n# Running the simulation of " << simulation_duration.count()
		<< " minute(s)...\n\n";

	logic::Light_algorithm_executor algorithm {intersection.second};
	logic::Simulator simulator {intersection.first, algorithm};

	auto time = util::timed_invoke([&]
	{
		simulator.run(simulation_duration, logic::debug_callback, *os);
	});

	std::cout << "# Finished the simulation; execution time: "
		<< std::chrono::duration_cast<std::chrono::minutes>(time).count()
		<< ":" << std::setw(2) << std::setfill('0')
		<< std::chrono::duration_cast<std::chrono::seconds>(time).count() % 60
		<< "." << std::setw(3) << std::setfill('0')
		<< std::chrono::duration_cast<std::chrono::milliseconds>(time)
			.count() % 1000
		<< std::setfill(' ') << '\n';

	auto count = std::accumulate(
		simulator.get_traffic().begin(), simulator.get_traffic().end(),
		0u, [](unsigned acc, auto& t)
	{
		return acc + t.second.size();
	});

	auto simulation_duration_sec
		= std::chrono::duration_cast<std::chrono::seconds>(
			simulation_duration).count();

	auto column_width = 20;
	auto description_column_width = 25;
	auto count_column_width = 5;

	std::cout
		<< std::setw(column_width) << std::left << "source lanes:"
		<< simulator.get_traffic().size() << '\n'

		<< std::setw(column_width) << std::left << "participants:"
		<< count
		<< " / "
		<< simulation_duration_sec << " sec = "
		<< std::fixed
		<< static_cast<double>(count)
			/ static_cast<double>(simulation_duration_sec) << " per sec\n";

	for (auto& t : simulator.get_traffic())
	{
		std::cout << std::setw(column_width) << std::left
			<< t.first
			<< std::setw(description_column_width) << std::left << "sources"
			<< std::setw(count_column_width) << std::right
			<< t.second.size() << " participant(s)\n";
	}

	std::unordered_map<model::Lane_id, unsigned> destination_map;

	for (auto& t : simulator.get_traffic())
	{
		for (auto& d : t.second)
			++destination_map[d.get_destination().id];
	}

	for (auto& d : destination_map)
	{
		std::cout << std::setw(column_width) << std::left
			<< d.first
			<< std::setw(description_column_width) << std::left
			<< "is the destination for"
			<< std::setw(count_column_width) << std::right << d.second
			<< " participant(s)\n";
	}

	if (output_file)
	{
		output_file << std::flush;
		output_file.close();
	}

	if (!analyze_path.empty())
	{
		std::cout << "Starting the analyzer '" << analyze_path
			<< "' for the output path: '" << output_path << "'\n";

		std::cout << "success = " << std::boolalpha
			<< cli::create_process(analyze_path, {output_path}) << '\n';
	}
}

int main(int argc, char* argv[])
{
	std::vector<std::string> args {argv, argv + argc};

	std::chrono::minutes simulation_duration {5};
	std::string input_path;
	std::string output_path;
	std::string analyze_path;

	for (auto it = args.begin(); it != args.end(); ++it)
	{
		if (*it == "simulation_duration")
		{
			if (it + 1 == args.end())
				throw std::invalid_argument {
					"simulation_duration requires 1 arg"};

			auto duration = std::stoi(*(++it));
			simulation_duration = std::chrono::minutes {duration};

			std::cout << "# Duration set to "
				<< str::to_str(simulation_duration, str::fmt::min_sec) << "'\n";
		}

		if (*it == "input")
		{
			if (it + 1 == args.end())
				throw std::invalid_argument {
					"input requires 1 arg"};

			input_path = *(++it);
			std::cout << "# Input path set to '" << input_path << "'\n";
		}

		if (*it == "output")
		{
			if (it + 1 == args.end())
				throw std::invalid_argument {
					"output requires 1 arg"};

			output_path = *(++it);

			std::cout << "# Output path set to '" << output_path << "'\n";
		}

		if (*it == "analyze")
		{
			if (it + 1 == args.end())
				throw std::invalid_argument {
					"analyze requires 1 arg"};

			analyze_path = *(++it);

			std::cout << "# Analyze path set to '" << analyze_path << "'\n";
		}

#ifdef ENABLE_REST
		if (*it == "server")
		{
			std::cout << "Starting the server..." << std::endl;

			using Http_server = SimpleWeb::Server<SimpleWeb::HTTP> ;
			Http_server server(8080, 1);

			server.resource["^/simulate$"]["POST"] =
				[=](Http_server::Response& response,
					std::shared_ptr<Http_server::Request> request)
			{
				std::cout << "/simulate POST request\n";

				std::stringstream ss;
				ss << request->content.rdbuf();

				std::cout << "\tLoaded the request's content\n";
				std::cout << "The file:\n"
					<< ss.str();

				std::stringstream output_ss;

				try
				{
					run_simulation(ss, output_ss, simulation_duration);
				}
				catch (std::exception& e)
				{
					std::cout << "Error, invalid file: " << e.what()
						<< std::endl;
				}
				auto content = output_ss.str();

				response << "HTTP/1.1 200 OK\r\nContent-Length: "
					<< content.length() << "\r\n\r\n" << content;

				std::cout << "Response set\n";
			};

			server.resource["^/info$"]["GET"] =
				[](Http_server::Response& response,
				std::shared_ptr<Http_server::Request> request)
			{
				std::cout << "/info GET request\n";

				std::stringstream content_stream;
				content_stream << "<h1>Request from "
					<< request->remote_endpoint_address << " ("
					<< request->remote_endpoint_port << ")</h1>"
					<< request->method << " " << request->path
					<< " HTTP/" << request->http_version << "<br>";
				for (auto& header: request->header)
					content_stream << header.first << ": "
						<< header.second << "<br>";

				content_stream.seekp(0, std::ios::end);

				response <<  "HTTP/1.1 200 OK\r\nContent-Length: "
					<< content_stream.tellp() << "\r\n\r\n"
					<< content_stream.rdbuf();
			};

			server.start();
		}

		if (*it == "client")
		{
			if (it + 1 == args.end())
				throw std::invalid_argument {
					"input requires 2 args, got 0"};

			if (it + 2 == args.end())
				throw std::invalid_argument {
					"input requires 2 args, got 1"};

			auto address = *(++it);
			input_path = *(++it);

			using Http_client = SimpleWeb::Client<SimpleWeb::HTTP> ;
			Http_client client(address);

			std::ifstream input_file {input_path};
			std::string file;
			std::string line;
			while (std::getline(input_file, line))
			{
				if (line.empty())
					continue;
				file += line + '\n';
			}
			std::cout << "Loaded the file '" << input_path << "':\n"
				<< file << '\n';

			auto response = client.request("POST", "/simulate", file);
			std::cout << "The reponse:\n"
				<< response->content.rdbuf() << std::endl;

			return 0;
		}
#endif // #ifdef ENABLE_REST
	}

	std::stringstream ss {
		R"(
		intersection Test konfliktów
		segment north Impreza o północy
			lane n_0 car_lane flow hot 3 0.5 cold 5 0 duration 10 0 size 4
				destination e_0
			lane n_1 car_lane
			lane n_2 car_lane
			lane n_c_0 crosswalk flow hot 6.5 0.5 cold 8.3 1.0 duration 60 0 size 10
				cross n_0
				destination n_c_1
			lane n_c_1 crosswalk flow hot 3 0 cold 5 0
				cross n_1
				destination n_c_1 0.5
				destination n_c_2 0.5
			lane n_c_2 crosswalk flow hot 2 0 cold 4 0
				cross n_2
				destination n_c_1
		segment east
			lane e_0 car_lane
		light n_c_0 10 0 10 2 0
		light n_c_1 20 0 15 2 5
		light n_c_2 10 0 10 2 0
		light n_0 5 3 30 3 0
		)"
	};

	std::ifstream fs {input_path};
	if (!input_path.empty() && !fs)
		throw std::runtime_error {"error, the path \"" + input_path + "\" is "
			"either invalid or inaccessible"};

	run_simulation(input_path.empty() ? dynamic_cast<std::istream&>(ss) : fs,
		output_path, simulation_duration, analyze_path);
}
