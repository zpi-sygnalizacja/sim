#include "cli/create_process.hpp"

#ifdef CLI_WIN32
	#include <windows.h>
#else
	#include <unistd.h>
	#include <sys/types.h>
	#include <sys/wait.h>
	#include <cstdio>
#endif

//std
#include <stdexcept>

bool cli::create_process(
	std::string path,
	std::vector<std::string> args)
{
#ifdef CLI_WIN32
	std::string cmd_line = '"' + path + "\" ";
#else
	std::vector<char*> c_args;
	c_args.reserve(args.size() + 1);
	c_args.push_back(&path[0]);
#endif
	for (auto& arg : args)
	{
#ifdef CLI_WIN32
		cmd_line += '"' + arg + "\" ";
#else
		c_args.push_back(&arg[0]);
#endif
	}

#ifndef CLI_WIN32
	c_args.push_back(nullptr);
#endif

#ifdef CLI_WIN32
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	// Start the child process.
	return CreateProcess(
		nullptr, //path.c_str(),   // No module name (use command line)
		&cmd_line[0],        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory
		&si,            // Pointer to STARTUPINFO structure
		&pi           // Pointer to PROCESS_INFORMATION structure
	);
#else
	auto pid = fork();
	auto e = errno;

	if (pid < 0)
		throw std::runtime_error {
			"Error: fork() failed, errno: " + std::to_string(e)};
	auto is_child = !pid;

	if (is_child)
	{
		execvp(path.c_str(), &c_args[0]);
		e = errno;
		throw std::runtime_error {"Error, execvp failed for '" + path
			+ "', errno: " + std::to_string(e)};
	}
	return true;
#endif
}
