#pragma once

#include <string>
#include <vector>

namespace cli
{
	bool create_process(
		std::string path,
		std::vector<std::string> args);
}
