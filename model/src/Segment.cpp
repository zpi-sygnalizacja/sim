// model
#include "model/Segment.hpp"

// std
#include <iostream>

std::ostream& model::operator<<(std::ostream& os, model::Direction direction)
{
	switch (direction)
	{
		case Direction::east:
			return os << "east";

		case Direction::north:
			return os << "north";

		case Direction::south:
			return os << "south";

		case Direction::west:
			return os << "west";

	#ifndef __clang__
		default:
			throw std::logic_error {"impossible"};
	#endif
	}
}

std::ostream& model::operator<<(std::ostream& os, const model::Segment& s)
{
	os << "segment " << s.direction;
	for (auto& lane : s.lanes)
		os << lane;

	return os;
}
