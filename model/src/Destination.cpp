// model
#include "model/Destination.hpp"
#include "model/Lane.hpp"

// std
#include <iostream>
#include <stdexcept>

std::ostream&model::operator <<(std::ostream& os, model::Destination::Type t)
{
	switch (t)
	{
		case Destination::Type::directed:
			return os << "directed";

		case Destination::Type::crossing:
			return os << "crossing";

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case"};
		#endif
	}
}

std::ostream& model::operator <<(std::ostream& os, const Destination& d)
{
	switch (d.type)
	{
		case Destination::Type::directed:
			return os << "destination " << d.lane.id << ' ' << d.probability;

		case Destination::Type::crossing:
			return os << "cross " << d.lane.id;

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case"};
		#endif
	}
}
