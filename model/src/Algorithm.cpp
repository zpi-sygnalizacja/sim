// model
#include "model/Algorithm.hpp"
#include "model/Lane.hpp"

// std
#include <iostream>

std::ostream& model::operator <<(
	std::ostream& os,
	const Algorithm_light& algorithm_light)
{
	return os << "light " << algorithm_light.lane.id << ' '
		<< algorithm_light.stop_duration.count() << ' '
		<< algorithm_light.prepare_to_move_duration.count() << ' '
		<< algorithm_light.move_duration.count() <<	' '
		<< algorithm_light.prepare_to_stop_duration.count() << ' '
		<< algorithm_light.stop_phase.count();
}

std::ostream& model::operator <<(std::ostream& os, const Algorithm& algorithm)
{
	for (auto& light : algorithm)
		os << light << '\n';
	return os;
}
