// model
#include "model/Light.hpp"

// std
#include <iostream>

std::ostream& model::operator<<(std::ostream& os, model::Light light)
{
	switch (light)
	{
		case Light::no_light:
			return os << "no_light";

		case Light::move:
			return os << "move";

		case Light::prepare_to_move:
			return os << "prepare_to_move";

		case Light::prepare_to_stop:
			return os << "prepare_to_stop";

		case Light::stop:
			return os << "stop";

	#ifndef __clang__
		default:
			throw std::logic_error {"impossible"};
	#endif
	}
}
