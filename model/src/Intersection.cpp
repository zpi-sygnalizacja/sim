// model
#include "model/Intersection.hpp"

// std
#include <algorithm>
#include <iostream>

model::Segment& model::Intersection::operator[](Direction direction)
{
	return *iterator_to(direction);
}

model::Intersection::Segments::iterator model::Intersection::iterator_to(
	Direction direction)
{
	return std::find_if(segments.begin(), segments.end(),
		[direction](auto& s)
	{
		return s.direction == direction;
	});
}

std::ostream& model::operator <<(std::ostream& os, const model::Intersection& i)
{
	os << "intersection " << i.name << '\n';
	for (auto& segment : i.segments)
		os << segment << '\n';

	return os;
}
