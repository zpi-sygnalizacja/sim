// model
#include "model/Lane.hpp"

// std
#include <iostream>

std::ostream& model::operator <<(std::ostream& os, Lane_type lt)
{
	switch (lt)
	{
		case Lane_type::bicycle_lane:
			return os << "bicycle_lane";

		case Lane_type::bus_lane:
			return os << "bus_lane";

		case Lane_type::car_lane:
			return os << "car_lane";

		case Lane_type::crosswalk:
			return os << "crosswalk";

		case Lane_type::tram_lane:
			return os << "tram_lane";

	#ifndef __clang__
		default:
			throw std::logic_error {"impossible"};
	#endif
	}
}

std::ostream& model::operator <<(std::ostream& os, const Flow& f)
{
	auto to_seconds = [](Flow::Precise_duration t)
	{
		return static_cast<double>(t.count()) / 1000.;
	};

	return os
		<< "hot " << to_seconds(f.hot_move_time)
		<< ' ' << to_seconds(f.hot_move_time_std_deviation)
		<< " cold " << to_seconds(f.cold_move_time)
		<< ' ' << to_seconds(f.cold_move_time_std_deviation)
		<< " delay " << f.delay
		<< " period " << f.period << ' ' << f.period_std_deviation
		<< " duration " << f.duration << ' ' << f.duration_std_deviation
		<< " size " << f.size;
}


std::ostream& model::operator <<(std::ostream& os, const Lane& l)
{
	os << "\n\tlane " << l.id << ' ' << l.lane_type;

	if (l.flow)
		os << " flow " << *l.flow;

	for (auto& destination : l.destinations)
		os << "\n\t\t" << destination;
	return os;
}
