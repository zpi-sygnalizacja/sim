#pragma once

// model
#include <model/Segment.hpp>

// std
#include <iosfwd>
#include <string>
#include <vector>

namespace model
{
	class Intersection
	{
	public:
		using Segments = std::vector<Segment>;

		Segment& operator[](Direction direction);
		Segments::iterator iterator_to(Direction direction);

		std::string name;
		Segments segments;
	};

	std::ostream& operator <<(std::ostream& os, const Intersection& i);
}
