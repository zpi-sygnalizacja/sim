#pragma once

// std
#include <iosfwd>

namespace model
{
	class Lane;

	class Destination
	{
	public:
		enum class Type
		{
			directed,
			crossing
		};

		Lane& lane;
		double probability;
		Type type;
	};

	std::ostream& operator <<(std::ostream& os, Destination::Type t);
	std::ostream& operator <<(std::ostream& os, const Destination& d);
}
