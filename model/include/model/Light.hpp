#pragma once

// std
#include <iosfwd>

namespace model
{
	enum class Light
	{
		no_light,
		stop,
		prepare_to_move,
		move,
		prepare_to_stop
	};

	std::ostream& operator<<(std::ostream& os, Light light);
}
