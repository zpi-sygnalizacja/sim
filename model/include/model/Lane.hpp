#pragma once

// model
#include <model/Destination.hpp>
#include <model/Lane_id.hpp>
#include <model/Light.hpp>

// util
#include <util/strong_typedef.hpp>

// std
#include <chrono>
#include <iosfwd>
#include <memory>
#include <vector>

namespace model
{
	// warning: be careful when trying to reorder the enumerators;
	// they're sorted according to their priority (lowest first)
	enum class Lane_type
	{
		car_lane,
		bus_lane,
		tram_lane,
		bicycle_lane,
		crosswalk
	};

	std::ostream& operator <<(std::ostream& os, Lane_type lt);

	struct Flow
	{
		using Duration = double;
		using Duration_std_deviation = double;
		using Size = double;
		using Size_std_deviation = double;

		Duration delay;

		Duration period;
		Duration_std_deviation period_std_deviation;

		Duration duration;
		Duration_std_deviation duration_std_deviation;

		Size size;

		using Precise_duration = std::chrono::milliseconds;

		Precise_duration hot_move_time;
		Precise_duration hot_move_time_std_deviation;
		Precise_duration cold_move_time;
		Precise_duration cold_move_time_std_deviation;
	};

	std::ostream& operator <<(std::ostream& os, const Flow& f);

	class Lane
	{
	public:
		using Destinations = std::vector<Destination>;

		Lane_id id;
		Lane_type lane_type;
		Light light {Light::no_light};
		std::unique_ptr<Flow> flow;
		Destinations destinations;
	};

	std::ostream& operator <<(std::ostream& os, const Lane& l);
}
