#pragma once

// logic

// std
#include <chrono>
#include <iosfwd>
#include <vector>

namespace model
{
	class Lane;

	class Algorithm_light
	{
	public:
		using Duration = std::chrono::seconds;
		Lane& lane;
		Duration stop_duration;
		Duration prepare_to_move_duration;
		Duration move_duration;
		Duration prepare_to_stop_duration;
		Duration stop_phase;
	};

	using Algorithm = std::vector<Algorithm_light>;

	std::ostream& operator <<(std::ostream& os,
		const Algorithm_light& algorithm_light);

	std::ostream& operator <<(std::ostream& os,
		const Algorithm& algorithm);
}
