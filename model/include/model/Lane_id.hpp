#pragma once

// util
#include <util/strong_typedef.hpp>

// std
#include <string>

namespace model
{
	namespace detail
	{
		struct Lane_id_tag
		{
		};
	}
	using Lane_id = util::strong_typedef<std::string, detail::Lane_id_tag>;
}
