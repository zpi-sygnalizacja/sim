#pragma once

//model
#include <model/Lane.hpp>

//std
#include <iosfwd>
#include <vector>

namespace model
{
	enum class Direction
	{
		north	= 0,
		east	= 1,
		south	= 2,
		west	= 3
	};

	std::ostream& operator <<(std::ostream& os, Direction direction);

	class Segment
	{
	public:
		using Lanes = std::vector<Lane>;

		Direction direction;
		Lanes lanes;
	};

	std::ostream& operator <<(std::ostream& os, const Segment& s);
}
