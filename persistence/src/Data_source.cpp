// persistence
#include "persistence/Data_source.hpp"

// str
#include <str/to_str.hpp>

// unit
#include <unit/pre.hpp>

// std
#include <algorithm>
#include <iostream>
#include <sstream>

using namespace std::literals;

persistence::Data_source::Data_source(std::istream& is)
	: is_ {is}
{
}

std::pair<model::Intersection, model::Algorithm>
persistence::Data_source::load()
{
	model::Intersection intersection {};
	model::Algorithm algorithm {};

	while (is_)
	{
		auto token = read_line();
		auto& line = std::get<std::string>(token);

		switch (std::get<Token_type>(token))
		{
			case Token_type::eof:
				apply_destinations(intersection);
				apply_algorithm(algorithm);
				return {std::move(intersection), std::move(algorithm)};

			case Token_type::unknown:
				throw std::runtime_error {"Invalid token: " + token.second};

			case Token_type::intersection:
				parse_intersection(intersection, line);
				break;

			case Token_type::segment:
				parse_segment(intersection, line);
				break;

			case Token_type::lane:
				parse_lane(intersection, line);
				break;

			case Token_type::destination:
				parse_destination(line);
				break;

			case Token_type::cross:
				parse_cross(line);
				break;

			case Token_type::algorithm_light:
				parse_algorithm(line);
				break;

			case Token_type::empty_line:
			case Token_type::comment:
				// ignore
				break;

		#ifndef __clang__
			default:
				throw std::logic_error {"impossible"};
		#endif
		}
	}

	throw std::logic_error {"impossible"};
}

std::pair<persistence::Data_source::Token_type, std::string>
persistence::Data_source::read_line()
{
	++line_no_;

	if (!is_)
		return std::make_pair(Token_type::eof, "invalid is_"s);

	std::pair<Token_type, std::string> token;

	if (!std::getline(is_ >> std::ws, token.second))
		return std::make_pair(Token_type::eof, "EOF @ getline");

	if (token.second.empty())
	{
		token.first = Token_type::empty_line;
		return token;
	}

	std::stringstream ss {token.second};
	ss.exceptions(ss.failbit | ss.badbit);

	ss >> token.second; // read first word

	if (token.second == "intersection")
		token.first = Token_type::intersection;
	else if (token.second == "segment")
		token.first = Token_type::segment;
	else if (token.second == "lane")
		token.first = Token_type::lane;
	else if (token.second == "destination")
		token.first = Token_type::destination;
	else if (token.second == "cross")
		token.first = Token_type::cross;
	else if (token.second == "light")
		token.first = Token_type::algorithm_light;
	else if (token.second.find("#") == 0u)
		token.first = Token_type::comment;
	else
		token.first = Token_type::unknown;

	if (!ss.eof())
		std::getline(ss >> std::ws, token.second); // read the rest of the line
	return token;
}

void persistence::Data_source::parse_intersection(
	model::Intersection& intersection,
	const std::string& line)
{
	intersection.name = line;
}

void persistence::Data_source::parse_segment(
	model::Intersection& intersection,
	const std::string& line)
{
	std::stringstream ss {line};
	std::string direction_string;
	ss >> direction_string;

	model::Direction direction;
	if (direction_string == "north")
		direction = model::Direction::north;
	else if (direction_string == "south")
		direction = model::Direction::south;
	else if (direction_string == "east")
		direction = model::Direction::east;
	else if (direction_string == "west")
		direction = model::Direction::west;
	else
		throw std::runtime_error {"Invalid direction " + direction_string};

	auto i = intersection.iterator_to(direction);
	if (i == intersection.segments.end())
		intersection.segments.push_back({direction, {}});

	current_segment_ = &intersection[direction];
}

void persistence::Data_source::parse_lane(
	model::Intersection& /*intersection*/,
	const std::string& line)
{
	unit::pre<std::runtime_error>(current_segment_, "Invalid file format;"
	" 'segment'' must preceed 'lane'");

	std::stringstream ss {line};
	ss.exceptions(ss.failbit | ss.failbit);

	model::Lane lane;

	ss >> lane.id;
	if (lane_ids_.find(lane.id) != lane_ids_.end())
		throw std::runtime_error {"Lane_id not unique: " + lane.id.value};
	else
		lane_ids_.insert(lane.id);

	std::string lane_type_str;
	ss >> lane_type_str;

	if (lane_type_str == "bicycle_lane")
		lane.lane_type = model::Lane_type::bicycle_lane;
	else if (lane_type_str == "bus_lane")
		lane.lane_type = model::Lane_type::bus_lane;
	else if (lane_type_str == "car_lane")
		lane.lane_type = model::Lane_type::car_lane;
	else if (lane_type_str == "crosswalk")
		lane.lane_type = model::Lane_type::crosswalk;
	else if (lane_type_str == "tram_lane")
		lane.lane_type = model::Lane_type::tram_lane;
	else
		throw std::runtime_error {"Invalid Lane_type: " + lane_type_str};

	auto manual_throw = false;
	std::string flow_str;
	try
	{
		if (!ss.eof() && ss >> flow_str)
		{
			if (flow_str == "flow")
			{
				lane.flow = std::make_unique<model::Flow>();
				parse_flow(*lane.flow, ss);
			}
			else
			{
				manual_throw = true;
				throw std::runtime_error {"Invalid token, expected 'flow', got '"
					+ flow_str + "'"};
			}
		}

	}
	catch (std::exception&)
	{
		if (ss.eof() && !manual_throw)
			; //ignore some whitespace
		else
			throw;
	}

	current_segment_->lanes.push_back(std::move(lane));
	current_lane_ = &current_segment_->lanes.back();
}

void persistence::Data_source::parse_flow(
	model::Flow& flow,
	std::stringstream& line)
{
	std::string word;

	auto manual_throw = false;

	try
	{
		while (!line.eof() && line >> word)
		{
			if (word == "hot")
			{
				double hot_move_time, hot_move_time_std_deviation;
				line >> hot_move_time >> hot_move_time_std_deviation;
				flow.hot_move_time = model::Flow::Precise_duration {
					static_cast<int>(hot_move_time * 1000)};
				flow.hot_move_time_std_deviation
					= model::Flow::Precise_duration {
						static_cast<int>(hot_move_time_std_deviation * 1000)};
			}
			else if (word == "cold")
			{
				double cold_move_time, cold_move_time_std_deviation;
				line >> cold_move_time >> cold_move_time_std_deviation;
				flow.cold_move_time = model::Flow::Precise_duration {
					static_cast<int>(cold_move_time * 1000)};
				flow.cold_move_time_std_deviation
					= model::Flow::Precise_duration {
						static_cast<int>(cold_move_time_std_deviation * 1000)};
			}
			else if (word == "delay")
				line >> flow.delay;
			else if (word == "period")
				line >> flow.period >> flow.period_std_deviation;
			else if (word == "duration")
				line >> flow.duration >> flow.duration_std_deviation;
			else if (word == "size")
				line >> flow.size;
			else
			{
				manual_throw = true;
				throw std::runtime_error {"Invalid token: " + word};
			}
		}
	}
	catch (...)
	{
		if (line.eof() && !manual_throw)
			; // ignore some white characters at the end of the line
		else
			throw;
	}
}

void persistence::Data_source::parse_algorithm(const std::string& line)
{
	std::stringstream ss {line};
	ss.exceptions(ss.failbit | ss.failbit);

	model::Lane_id lane_id;
	unsigned stop_duration;
	unsigned prepare_to_move_duration;
	unsigned move_duration;
	unsigned prepare_to_stop_duration;
	unsigned stop_phase;

	ss >> lane_id >> stop_duration >> prepare_to_move_duration
		>> move_duration >> prepare_to_stop_duration >> stop_phase;

	Algorithm_light light {
		lane_id,
		model::Algorithm_light::Duration {stop_duration},
		model::Algorithm_light::Duration {prepare_to_move_duration},
		model::Algorithm_light::Duration {move_duration},
		model::Algorithm_light::Duration {prepare_to_stop_duration},
		model::Algorithm_light::Duration {stop_phase}
	};

	algorithm_.push_back(light);
}

void persistence::Data_source::parse_destination(
	const std::string& line,
	model::Destination::Type type)
{
	unit::pre<std::runtime_error>(current_lane_,
		"Token 'lane' must preceed token 'destination'");

	Destination destination {};

	std::stringstream ss {line};
	ss.exceptions(ss.failbit | ss.badbit);

	ss >> destination.lane_id;
	if (type == model::Destination::Type::directed)
	{
		if (!ss.eof())
			ss >> destination.probability;
		else
			destination.probability = 1.;
	}
	destination.type = type;
	destinations_[current_lane_->id].push_back(destination);
}

void persistence::Data_source::parse_cross(const std::string& line)
{
	unit::pre<std::runtime_error>(current_lane_,
		"Token 'lane' must preceed token 'cross'");
	unit::pre<std::runtime_error>(current_lane_->lane_type
		== model::Lane_type::crosswalk,
		"Token 'cross' can only follow a 'lane' of type 'crosswalk'; lane id: "
		+ current_lane_->id.value + ", type: "
		+ str::to_str(current_lane_->lane_type));

	parse_destination(line, model::Destination::Type::crossing);
}

void persistence::Data_source::apply_destinations(
	model::Intersection& intersection)
{
	for (auto& segment : intersection.segments)
	{
		for (auto& lane : segment.lanes)
			all_lanes_[lane.id] = &lane;
	}

	for (auto& destination : destinations_)
	{
		std::sort(destination.second.begin(),
			destination.second.end(), [](auto& l, auto& r)
		{
			return l.type == model::Destination::Type::directed
				&& r.type == model::Destination::Type::crossing;
		});

		for (auto& target : destination.second)
		{
			unit::pre<std::runtime_error>(all_lanes_[target.lane_id],
				"Invalid destination lane id: " + target.lane_id.value);

			unit::pre<std::runtime_error>(
				target.type != model::Destination::Type::crossing
				|| all_lanes_[target.lane_id]->lane_type
					!= model::Lane_type::crosswalk,
				"A 'cross' token cannot point to a lane of type 'crosswalk'; "
				"the target's id: " + target.lane_id.value);

			all_lanes_[destination.first]->destinations.push_back(
				{*all_lanes_[target.lane_id], target.probability, target.type});
		}
	}
}

void persistence::Data_source::apply_algorithm(model::Algorithm& algorithm)
{
	for (auto& algorithm_light : algorithm_)
	{
		unit::pre<std::runtime_error>(all_lanes_[algorithm_light.lane_id],
			"Invalid light lane id: " + algorithm_light.lane_id.value);

		algorithm.push_back({
			*all_lanes_[algorithm_light.lane_id],
			algorithm_light.stop_duration,
			algorithm_light.prepare_to_move_duration,
			algorithm_light.move_duration,
			algorithm_light.prepare_to_stop_duration,
			algorithm_light.stop_phase
		});
	}
}
