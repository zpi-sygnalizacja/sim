#pragma once

// model
#include <model/Algorithm.hpp>
#include <model/Intersection.hpp>

// std
#include <iosfwd>
#include <string>
#include <tuple>
#include <unordered_map>
#include <unordered_set>

namespace persistence
{
	class Data_source
	{
	public:
		Data_source(std::istream& is);
		std::pair<model::Intersection, model::Algorithm> load();

		auto get_line_no()
		{
			return line_no_;
		}

	private:
		enum class Token_type
		{
			intersection,
			segment,
			lane,
			destination,
			eof, // end of file
			unknown,
			algorithm_light,
			cross,
			empty_line,
			comment
		};

		struct Destination
		{
			model::Lane_id lane_id;
			double probability;
			model::Destination::Type type;
		};

		struct Algorithm_light
		{
			model::Lane_id lane_id;
			model::Algorithm_light::Duration stop_duration;
			model::Algorithm_light::Duration prepare_to_move_duration;
			model::Algorithm_light::Duration move_duration;
			model::Algorithm_light::Duration prepare_to_stop_duration;
			model::Algorithm_light::Duration stop_phase;
		};

		std::pair<Token_type, std::string> read_line();
		void parse_intersection(
			model::Intersection& intersection,
			const std::string& line);

		void parse_segment(
			model::Intersection& intersection,
			const std::string& line);

		void parse_lane(
			model::Intersection& intersection,
			const std::string& line);

		void parse_flow(
			model::Flow& flow,
			std::stringstream& line);

		void parse_algorithm(const std::string& line);
		void parse_destination(
			const std::string& line,
			model::Destination::Type type = model::Destination::Type::directed);
		void parse_cross(const std::string& line);

		void apply_destinations(model::Intersection& intersection);
		void apply_algorithm(model::Algorithm& algorithm);

		std::istream& is_;
		model::Segment* current_segment_ = nullptr;

		std::unordered_set<model::Lane_id> lane_ids_;
		model::Lane* current_lane_ = nullptr;
		std::size_t line_no_ {};

		std::unordered_map<model::Lane_id, model::Lane*> all_lanes_;
		std::unordered_map<model::Lane_id, std::vector<Destination>>
			destinations_;
		std::vector<Algorithm_light> algorithm_;
	};
}
