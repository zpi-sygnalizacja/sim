// logic
#include "logic/Traffic_participant.hpp"

// std
#include <algorithm>
#include <iostream>
#include <stdexcept>

logic::Traffic_participant::Traffic_participant(
	const model::Lane& source,
	const model::Lane& destination,
	model::Lane_type type,
	const Conflicting_lanes& conflicting_lanes,
	Resolution move_duration,
	Resolution delay_duration)
	:
	source_ {source},
	destination_ {destination},
	conflicting_lanes_ {conflicting_lanes},
	type_ {type},
	delay_duration_ {delay_duration},
	initial_delay_duration_ {delay_duration},
	move_duration_left_ {move_duration},
	id_ {next_id()}
{
}

void logic::Traffic_participant::make_step(
	Resolution resolution,
	const Traffic& current_traffic)
{
	if (type_ == Type::crosswalk)
		make_pedestrian_step(resolution);
	else
		make_vehicle_step(resolution, current_traffic);
}

const model::Lane& logic::Traffic_participant::get_source() const
{
	return source_.get();
}

const model::Lane& logic::Traffic_participant::get_destination() const
{
	return destination_.get();
}

logic::Traffic_participant::State logic::Traffic_participant::get_state() const
{
	return state_;
}

logic::Traffic_participant::Id logic::Traffic_participant::get_id() const
{
	return id_;
}

model::Lane_type logic::Traffic_participant::get_type() const
{
	return type_;
}

logic::Traffic_participant::Id logic::Traffic_participant::next_id()
{
	thread_local Id id {};
	return id++;
}

void logic::Traffic_participant::make_pedestrian_step(Resolution resolution)
{
	switch (state_)
	{
		case State::created:
			state_ = source_.get().light == model::Light::move
				|| source_.get().light == model::Light::prepare_to_stop
				? State::moving : State::waiting;
			break;

		case State::waiting:
			state_ = source_.get().light == model::Light::move
				|| source_.get().light == model::Light::prepare_to_move
				? State::delayed_moving : State::waiting;
			break;

		case State::delayed_moving:
			delay_duration_ -= resolution;
			if (delay_duration_ <= delay_duration_.zero())
				state_ = State::moving;
			break;

		case State::moving:
			move_duration_left_ -= resolution;
			if (move_duration_left_ <= move_duration_left_.zero())
				state_ = State::left_intersection;
			break;

		case State::left_intersection:
			break;

		case State::blocked:
			throw std::logic_error {"Invalid case"};

		#ifndef __clang__
		default:
			throw std::logic_error {"Invalid case"};
		#endif
	}
}

void logic::Traffic_participant::make_vehicle_step(
	Resolution resolution,
	const logic::Traffic_participant::Traffic& current_traffic)
{
	auto pre_tp = pre(current_traffic);

	(void)resolution;

	switch (state_)
	{
		case State::created:
			state_ = (source_.get().light == model::Light::move
				|| source_.get().light == model::Light::prepare_to_stop)
				&& (!pre_tp || pre_tp->get_state() == State::moving)
				&& !conflict_at_crossing_crosswalk(current_traffic)
				? State::moving : State::waiting;

			break;

		case State::waiting:
			state_ = source_.get().light == model::Light::move
				|| source_.get().light == model::Light::prepare_to_move
				? State::delayed_moving : State::waiting;
			break;

		case State::delayed_moving:
			delay_duration_ -= resolution;
			if (delay_duration_ <= delay_duration_.zero())
			{
				state_ = (source_.get().light == model::Light::move
					||
					source_.get().light ==	model::Light::prepare_to_stop)
					&& (!pre_tp || pre_tp->get_state() == State::moving)
					&& !conflict_at_crossing_crosswalk(current_traffic)
					? State::moving : State::waiting;
				if (state_ == State::waiting)
					delay_duration_ = initial_delay_duration_;
			}
			break;

		case State::moving:
			if (!conflict(current_traffic))
				move_duration_left_ -= resolution;
			else
				state_ = State::blocked;

			if (move_duration_left_ <= move_duration_left_.zero())
				state_ = State::left_intersection;
			break;

		case State::left_intersection:
			break;

		case State::blocked:
			if (!conflict(current_traffic))
				state_ = State::moving;
			break;

		#ifndef __clang__
		default:
			throw std::logic_error {"Invalid case"};
		#endif
	}
}

const logic::Traffic_participant* logic::Traffic_participant::pre(
	const Traffic_participant::Traffic& traffic)
{
	const Traffic_participant* last {};
	for (auto& tp : traffic.at(source_.get().id))
	{
		if (&tp == this)
			return last;
		else
			last = &tp;
	}
	throw std::logic_error {"Must not reach here"};
}

bool logic::Traffic_participant::conflict_at_crossing_crosswalk(
	const logic::Traffic_participant::Traffic& traffic)
{
	for (auto& cl : conflicting_lanes_.get())
	{
		if (std::get<0>(cl).get().lane_type == model::Lane_type::crosswalk
			&& std::get<1>(cl).get().type == model::Destination::Type::crossing
			&& std::get<1>(cl).get().lane.id == source_.get().id)
		{
			auto p = traffic.find(std::get<0>(cl).get().id);
			if (p == traffic.end())
				return false;

			return std::find_if(p->second.begin(), p->second.end(),
				[](auto& tp)
			{
				return tp.get_state() == State::moving;
			}) != p->second.end();
		}
	}
	return false;
}

bool logic::Traffic_participant::conflict(const Traffic& traffic)
{
	for (auto& cl : conflicting_lanes_.get())
	{
		if (std::get<0>(cl).get().lane_type == model::Lane_type::crosswalk
			&& std::get<1>(cl).get().type == model::Destination::Type::crossing
			&& std::get<1>(cl).get().lane.id == source_.get().id)
			continue;
			// skip the crossing crosswalk since the vehicle passed it

		auto p = traffic.find(std::get<0>(cl).get().id);
		if (p == traffic.end())
			return false;

		auto r = std::find_if(p->second.begin(), p->second.end(),
			[this, &cl](auto& tp)
		{
			return tp.get_state() == State::moving
				&& (tp.get_type() > this->get_type() || (
					tp.get_type() == this->get_type() && std::get<2>(cl)
				));
		});

		if (r != p->second.end())
			return true;
	}
	return false;
}

std::ostream& logic::operator <<(std::ostream& os, Traffic_participant::State s)
{
	using S = Traffic_participant::State;

	switch (s)
	{
		case S::created:
			return os << "created";

		case S::left_intersection:
			return os << "left_intersection";

		case S::blocked:
			return os << "blocked";

		case S::moving:
			return os << "moving";

		case S::waiting:
			return os << "waiting";

		case S::delayed_moving:
			return os << "delayed_moving";

		#ifndef __clang__
		default:
			throw std::logic_error {"Invalid case"};
		#endif
	}
}
