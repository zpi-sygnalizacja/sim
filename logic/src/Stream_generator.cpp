// logic
#include "logic/Stream_generator.hpp"
#include "logic/Simulator.hpp"

// model
#include <model/Intersection.hpp>

// unit
#include <unit/post.hpp>

// util
#include <util/thread_prng.hpp>

// std
#include <algorithm>
#include <stdexcept>

namespace
{
	decltype (auto) direction_of_lane(
		const model::Intersection& intersection,
		const model::Lane& lane)
	{
		for (auto& s : intersection.segments)
		{
			for (auto& l : s.lanes)
			{
				if (l.id == lane.id)
					return s.direction;
			}
		}
		throw std::invalid_argument {"Invalid format"};
	}

	auto get_lane(
		const model::Intersection& intersection,
		model::Direction direction) -> const model::Segment*
	{
		for (auto& s : intersection.segments)
		{
			if (s.direction == direction)
				return &s;
		}
		return nullptr;
	}
}

logic::Stream_generator::Stream_generator(
	const model::Intersection& intersection,
	Traffic& traffic)
{
	for (auto& segment : intersection.segments)
	{
		for (auto& lane : segment.lanes)
		{
			if (lane.flow)
				lane_stream_generators_.emplace_back(intersection, lane,
					traffic);
		}
	}
}

void logic::Stream_generator::run_step(Resolution resolution)
{
	for (auto& gen : lane_stream_generators_)
		gen.run_step(resolution);
}

logic::Stream_generator::Lane_stream_generator::Lane_stream_generator(
	const model::Intersection& intersection,
	const model::Lane& lane,
	Traffic& traffic)
	: lane_ {lane}, traffic_ {traffic},
	state_duration_ {Resolution {static_cast<int>(lane.flow->delay * 1000.)}},
	size_distribution_{lane.flow->size
		/ (lane.flow->duration * 1000.
			/ static_cast<double>(Simulator::resolution().count()))}
{
	std::vector<double> probabilities;

	auto last_destination = std::find_if_not(lane_.destinations.begin(),
		lane_.destinations.end(), [](auto& d)
	{
		return d.type == model::Destination::Type::directed;
	});

	std::transform(lane_.destinations.begin(), last_destination,
		std::back_inserter(probabilities),
		[](const model::Destination& d)
	{
		return d.probability;
	});
	destination_distribution_ = decltype (destination_distribution_){
		probabilities.begin(), probabilities.end()};

	generate_conflicting_lanes(intersection);
}

void logic::Stream_generator::Lane_stream_generator::run_step(
	Resolution resolution)
{
	switch (state_)
	{
		case State::initial_phase:
			state_duration_ -= resolution;
			if (state_duration_ <= state_duration_.zero())
			{
				state_ = State::in_wave;
				generate_wave();
			}
			break;

		case State::in_wave:
			if (size_distribution_(util::thread_prng()))
				generate_participant();

			state_duration_ -= resolution;
			if (state_duration_ <= state_duration_.zero())
			{
				state_ = State::outside_wave;
				generate_wave();
			}
			break;

		case State::outside_wave:
			state_duration_ -= resolution;
			if (state_duration_ <= state_duration_.zero())
			{
				state_ = State::in_wave;
				generate_wave();
			}
			break;

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case hit"};
		#endif
	}
}

const model::Segment*
logic::Stream_generator::Lane_stream_generator::get_segment(
	const model::Intersection& intersection,
	const model::Lane& lane,
	Turn turn)
{
	auto direction = static_cast<int>(direction_of_lane(intersection, lane));
	auto dir = (direction + static_cast<int>(turn)) % 4;
	if (dir == -1)
		return get_lane(intersection, model::Direction::west);
	else
	{
		unit::post<std::logic_error>(dir >= 0 && dir <= 3, "Invalid direction");
		return get_lane(intersection, static_cast<model::Direction>(dir));
	}
}

logic::Stream_generator::Lane_stream_generator::Turn
logic::Stream_generator::Lane_stream_generator::determine_turn_type(
	const model::Intersection& intersection,
	const model::Lane& lane,
	const model::Destination& destination)
{
	auto diff = static_cast<int>(direction_of_lane(intersection, lane))
		- static_cast<int>(direction_of_lane(intersection, destination.lane));

	if (std::abs(diff) == 0)
		return Turn::u_turn;
	else if (std::abs(diff) == 2)
		return Turn::straight;
	else
	{
		switch (diff)
		{
			case -3:
			case 1:
				return Turn::right;

			case -1:
			case 3:
				return Turn::left;

			default:
				throw std::logic_error {"Impossible case"};
		}
	}
}

void logic::Stream_generator::Lane_stream_generator::generate_wave()
{
	switch (state_)
	{
		case State::in_wave:
		{
			std::normal_distribution<> duration_dist {
				lane_.flow->duration, lane_.flow->duration_std_deviation};
			state_duration_ = Resolution {
				static_cast<int>(duration_dist(util::thread_prng()) * 1000.)};
			break;
		}

		case State::outside_wave:
		{
			std::normal_distribution<> period_dist {
				lane_.flow->period, lane_.flow->period_std_deviation};
			state_duration_ = Resolution {
				static_cast<int>(period_dist(util::thread_prng()) * 1000.)};
			break;
		}

		case State::initial_phase:
			throw std::logic_error {"Invalid state"};

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case hit"};
		#endif
	}
}

void logic::Stream_generator::Lane_stream_generator::generate_participant()
{
	//todo: use simulator parameters

	auto use_hot_time = lane_.light == model::Light::move
		|| lane_.light == model::Light::prepare_to_stop;

	std::normal_distribution<> move_duration_dist {
		static_cast<double>(use_hot_time
			? lane_.flow->hot_move_time.count()
			: lane_.flow->cold_move_time.count()),
		static_cast<double>(use_hot_time
			? lane_.flow->hot_move_time_std_deviation.count()
			: lane_.flow->cold_move_time_std_deviation.count()),
	};

	std::normal_distribution<> delay_duration_dist {1.0, 0.1};

	auto& destination = choose_destination_lane();
	traffic_[lane_.id].emplace_back(lane_, destination,
		lane_.lane_type, conflicting_lanes_[destination.id],
		Resolution {static_cast<long>(move_duration_dist(util::thread_prng()))},
		Resolution {static_cast<long>(delay_duration_dist(util::thread_prng()))}
	);
}

void logic::Stream_generator::Lane_stream_generator::generate_conflicting_lanes(
	const model::Intersection& intersection)
{
	for (auto& d : lane_.destinations)
	{
		if (lane_.lane_type != model::Lane_type::crosswalk)
		{
			find_crossing_crosswalks(intersection, d);
			auto turn_type = determine_turn_type(intersection, d);
			find_conflicting_turns(intersection, d, turn_type);
		}
		else
			find_crossed_lanes(intersection, d);
	}

	std::cout << "Conflicts summary for lane " << lane_.id << '\n';

	for (auto& cl : conflicting_lanes_)
	{
		std::cout
			<< "\tconflicting <lane, destination> pairs for destination lane: "
			<< cl.first << ", turn: "
			<< determine_turn_type(intersection, cl.first) << '\n';

		for (auto& l : cl.second)
			std::cout
				<< "\t\t" << std::get<0>(l).get().id << " of type "
				<< std::get<0>(l).get().lane_type
				<< " → " << std::get<1>(l).get().lane.id << " of type "
				<< std::get<1>(l).get().lane.lane_type << "; destination type: "
				<< std::get<1>(l).get().type << ", turn: "
				<< determine_turn_type(intersection, std::get<0>(l).get(),
					std::get<1>(l).get())
				<< ", conflicting lanes has priority = "
				<< std::boolalpha << std::get<2>(l)
				<< '\n';
	}
}

logic::Stream_generator::Lane_stream_generator::Turn
logic::Stream_generator::Lane_stream_generator::determine_turn_type(
	const model::Intersection& intersection,
	const model::Destination& destination)
{
	return determine_turn_type(intersection, lane_, destination);
}

logic::Stream_generator::Lane_stream_generator::Turn
logic::Stream_generator::Lane_stream_generator::determine_turn_type(
	const model::Intersection& intersection,
	model::Lane_id lane_id)
{
	for (auto& d : lane_.destinations)
	{
		if (d.lane.id == lane_id)
			return determine_turn_type(intersection, d);
	}
	throw std::logic_error {"Must not reach here"};
}

const model::Lane&
logic::Stream_generator::Lane_stream_generator::choose_destination_lane()
{
	auto directed_count = std::count_if(lane_.destinations.begin(),
		lane_.destinations.end(), [](auto& d)
	{
		return d.type == model::Destination::Type::directed;
	});
	if (!directed_count)
		return lane_;

	auto& destination
		= lane_.destinations[destination_distribution_(util::thread_prng())];

	unit::post<std::logic_error>(destination.type
		!= model::Destination::Type::crossing, "Invalid Destination type");

	return destination.lane;
}

void logic::Stream_generator::Lane_stream_generator::find_crossing_crosswalks(
	const model::Intersection& intersection,
	const model::Destination& destination)
{
	for (auto& s : intersection.segments)
	{
		for (auto& l : s.lanes)
		{
			if (l.lane_type != model::Lane_type::crosswalk)
				continue;

			for (auto& d : l.destinations)
			{
				if (d.type != model::Destination::Type::crossing)
					continue;

				if (d.lane.id == destination.lane.id || d.lane.id == lane_.id)
					conflicting_lanes_[destination.lane.id].emplace_back(l, d,
						true);
			}
		}
	}
}

void logic::Stream_generator::Lane_stream_generator::find_crossed_lanes(
	const model::Intersection& intersection,
	const model::Destination& destination)
{
	if (destination.type != model::Destination::Type::crossing)
		return;
	for (auto& d : destination.lane.destinations)
		conflicting_lanes_[destination.lane.id].emplace_back(
			destination.lane, d, false);

	for (auto& s : intersection.segments)
	{
		for (auto& l : s.lanes)
		{
			if (l.lane_type == model::Lane_type::crosswalk)
				continue;

			for (auto& d : l.destinations)
			{
				if (d.type == model::Destination::Type::crossing)
					continue;

				if (d.lane.id == destination.lane.id || d.lane.id == lane_.id)
					conflicting_lanes_[destination.lane.id].emplace_back(l, d,
						false);
			}
		}
	}
}

void logic::Stream_generator::Lane_stream_generator::find_conflicting_turns(
	const model::Intersection& intersection,
	const model::Destination& destination,
	Turn turn)
{
	auto for_each = [&](auto s, auto f)
	{
		if (!s)
			return;
		for (auto& la : s->lanes)
			for (auto& de : la.destinations)
				f(la, de);
	};

	auto add_conflict_same_direction = [&](auto s)
	{
		for_each(s, [&](auto& l, auto& d)
		{
			if (d.type == model::Destination::Type::crossing)
				return;

			if (direction_of_lane(intersection, d.lane)
				== direction_of_lane(intersection, destination.lane))
				conflicting_lanes_[destination.lane.id].emplace_back(l, d,
					false);
		});
	};

	auto add_conflict_direction = [&](auto s, Turn t, bool conflict_priority)
	{
		for_each(s, [&](auto& l, auto& d)
		{
			if (d.type == model::Destination::Type::crossing)
				return;

			if (determine_turn_type(intersection, l, d) == t)
				conflicting_lanes_[destination.lane.id].emplace_back(l, d,
					conflict_priority);
		});
	};

	auto add_conflict_all = [&](auto s)
	{
		for_each(s, [&](auto& l, auto& d)
		{
			if (d.type == model::Destination::Type::crossing)
				return;

			conflicting_lanes_[destination.lane.id].emplace_back(l, d, true);
		});
	};

	switch (turn)
	{
		case Turn::left:
		{
			auto right_segment = get_segment(intersection, lane_, Turn::right);
			add_conflict_direction(right_segment, Turn::straight, true);
			add_conflict_direction(right_segment, Turn::left, true);

			auto straight_segment = get_segment(intersection, lane_,
				Turn::straight);
			add_conflict_direction(straight_segment, Turn::right, true);
			add_conflict_direction(straight_segment, Turn::straight, true);

			auto left_segment = get_segment(intersection, lane_, Turn::left);
			add_conflict_direction(left_segment, Turn::straight, false);
			add_conflict_direction(left_segment, Turn::left, false);
			add_conflict_direction(left_segment, Turn::u_turn, false);

			break;
		}
		case Turn::right:
		{
			add_conflict_same_direction(
				get_segment(intersection, lane_, Turn::right));
			add_conflict_same_direction(
				get_segment(intersection, lane_, Turn::straight));
			add_conflict_same_direction(
				get_segment(intersection, lane_, Turn::left));

			break;
		}
		case Turn::straight:
		{
			add_conflict_all(get_segment(intersection, lane_, Turn::right));
			auto straight_segment = get_segment(intersection, lane_,
				Turn::straight);
			add_conflict_direction(straight_segment, Turn::left, false);
			add_conflict_direction(straight_segment, Turn::u_turn, false);

			auto left_segment = get_segment(intersection, lane_, Turn::left);
			add_conflict_direction(left_segment, Turn::left, false);
			add_conflict_direction(left_segment, Turn::straight, false);

			break;
		}
		case Turn::u_turn:
		{
			add_conflict_direction(get_segment(intersection, lane_,
				Turn::right), Turn::left, true);
			add_conflict_direction(get_segment(intersection, lane_,
				Turn::straight), Turn::straight, true);
			auto left_segment = get_segment(intersection, lane_, Turn::left);
			add_conflict_direction(left_segment, Turn::right, true);
			add_conflict_direction(left_segment, Turn::straight, true);

			break;
		}

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case hit"};
		#endif
	}
}

std::ostream& logic::operator <<(
	std::ostream& os,
	Stream_generator::Turn_type t)
{
	using T = Stream_generator::Turn_type;

	switch (t)
	{
		case T::left:
			return os << "left turn";

		case T::right:
			return os << "right turn";

		case T::straight:
			return os << "straight turn";

		case T::u_turn:
			return os << "u-turn";

		#ifndef __clang__
		default:
			throw std::logic_error {"Invalid case"};
		#endif
	}
}
