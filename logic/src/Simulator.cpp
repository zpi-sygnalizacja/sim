// logic
#include "logic/Simulator.hpp"

// std
#include <str/chrono_to_str.hpp>

// unit
#include <unit/pre.hpp>
#include <unit/post.hpp>

// util
#include <util/thread_prng.hpp>

// std
#include <algorithm>

namespace
{
	decltype (auto) get_segment(
		const model::Intersection& intersection,
		const model::Lane_id lane_id)
	{
		for (auto& s : intersection.segments)
			for (auto& l : s.lanes)
				if (l.id == lane_id)
					return s;
		throw std::invalid_argument {"Must not reach here"};
	}

	decltype (auto) get_lane(
		const model::Intersection& intersection,
		const model::Lane_id lane_id)
	{
		for (auto& s : intersection.segments)
			for (auto& l : s.lanes)
				if (l.id == lane_id)
					return l;
		throw std::invalid_argument {"Must not reach here"};
	}
}

logic::Simulator::Simulator(
	const model::Intersection& intersection,
	Light_algorithm_executor& algorithm)
	: intersection_ {intersection}, algorithm_ {algorithm},
	stream_generator_{intersection_, current_traffic_}
{
}

logic::Simulator::Resolution logic::Simulator::resolution()
{
	return Resolution {10};
}

const logic::Traffic& logic::Simulator::get_traffic()
{
	return current_traffic_;
}

void logic::Simulator::make_traffic_step()
{
	for (auto& t : current_traffic_)
	{
		for (auto& lt : t.second)
			lt.make_step(resolution(),
				current_traffic_);
	}
}

void logic::Simulator::handle_chained_crosswalks()
{
	//todo: use simulator parameters

	for (auto& t : current_traffic_)
	{
		auto& lane = get_lane(intersection_, t.first);
		if (lane.lane_type != model::Lane_type::crosswalk)
			continue;

		for (auto& lt : t.second)
		{
			if (lt.get_source().id == lt.get_destination().id
				||
				lt.get_destination().lane_type != model::Lane_type::crosswalk
				||
				lt.get_state() != Traffic_participant::State::left_intersection)
				continue;

			std::vector<double> probabilities;

			auto last_destination = std::find_if_not(
				lt.get_destination().destinations.begin(),
				lt.get_destination().destinations.end(), [](auto& d)
			{
				return d.type == model::Destination::Type::directed;
			});

			std::transform(lt.get_destination().destinations.begin(),
				last_destination,
				std::back_inserter(probabilities),
				[](const model::Destination& d)
			{
				return d.probability;
			});
			std::discrete_distribution<std::size_t> destination_distribution_ {
				probabilities.begin(), probabilities.end()};

			auto& destination
				= last_destination != lt.get_destination().destinations.begin()
				? lt.get_destination().destinations[
					destination_distribution_(util::thread_prng())].lane
				: lt.get_destination();

			auto& source = lt.get_destination();

			unit::pre<std::invalid_argument>(destination.flow,
				"No destination or no flow in destination; source crosswalk: "
				+ lane.id.value + ", destination crosswalk: "
				+ destination.id.value + "; fix the file");

			auto use_hot_time = destination.light == model::Light::move
				|| destination.light == model::Light::prepare_to_stop;


			std::normal_distribution<> move_duration_dist {
				static_cast<double>(use_hot_time
					? destination.flow->hot_move_time.count()
					: destination.flow->cold_move_time.count()),
				static_cast<double>(use_hot_time
					? destination.flow->hot_move_time_std_deviation.count()
					: destination.flow->cold_move_time_std_deviation.count()),
			};

			std::normal_distribution<> delay_duration_dist {1.0, 0.1};

			current_traffic_[source.id].emplace_back(source, destination,
				source.lane_type, Traffic_participant::Conflicting_lanes {},
				Resolution {static_cast<long>(
					move_duration_dist(util::thread_prng()))},
				Resolution {static_cast<long>(
					delay_duration_dist(util::thread_prng()))}
			);
		}
	}
}

void logic::Simulator::remove_old_traffic()
{
	for (auto& t : current_traffic_)
	{
		auto p = std::remove_if(t.second.begin(), t.second.end(),
			[](auto& lt)
		{
			return lt.get_state()
				== Traffic_participant::State::left_intersection;
		});

		t.second.erase(p, t.second.end());
	}
}

void logic::debug_callback(
	std::chrono::milliseconds t,
	const model::Intersection& intersection,
	const Traffic& traffic,
	std::ostream& os)
{
	if (t == t.zero())
	{
		os << "# format:\n"
			<< "# new_lane <lane_id> <segment_direction>"
			<< "# tp <time_point_millisec>\n"
			<< "# \tlane <lane_id> <segment>\n"
			<< "# \t\tnew_part <participant_id> <destination_lane> "
				"<destination_segment_direction> <type>\n"
			<< "# \t\told_part <participant_id>\n"
			<< "# \t\tpart <participant_id> <state>\n";

		for (auto& s : intersection.segments)
		{
			for (auto& l : s.lanes)
				os << "new_lane " << l.id << ' ' << s.direction << '\n';
		}
	}

	os << "tp " << t.count() << '\n';

	for (auto& tr : traffic)
	{
		os << "\tlane " << tr.first << '\n';
		for (auto& p : tr.second)
		{
			using S = Traffic_participant::State;

			switch (p.get_state())
			{
				case S::created:
					os << "\t\tnew_part " << p.get_id() << ' '
						<< p.get_destination().id << ' '
						<< get_segment(intersection, p.get_destination().id)
							.direction
						<< ' ' << p.get_type() << '\n';
					break;

				case S::left_intersection:
					os << "\t\told_part " << p.get_id() << '\n';
					break;

				case S::blocked:
				case S::moving:
				case S::waiting:
				case S::delayed_moving:
					os << "\t\tpart " << p.get_id() << ' '
						<< p.get_state() << '\n';
					break;

				#ifndef __clang__
				default:
					throw std::logic_error {"Impossible case hit"};
				#endif
			}
		}
	}

	if (t.count() % 1000)
		return;

	auto traffic_size = std::accumulate(
		traffic.begin(), traffic.end(), 0u, [](unsigned acc, auto& lt)
	{
		return acc + lt.second.size();
	});

	std::cout << "# STEP: "
		<< str::to_str(std::chrono::duration_cast<std::chrono::seconds>(t))
		<< ", traffic size: " << traffic_size
		<< '\n';

	for (auto& segment : intersection.segments)
	{
		for (auto& lane : segment.lanes)
		{
			std::cout << "\tLane " << lane.id << " is " << lane.light << '\n';
		}
	}
	std::cout << "# END OF STEP #\n\n";
}
