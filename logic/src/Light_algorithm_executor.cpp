// logic
#include "logic/Light_algorithm_executor.hpp"

// model
#include <model/Lane.hpp>

logic::Light_algorithm_executor::Light_algorithm_executor(
	const model::Algorithm& algorithm_model)
{
	for (auto& light : algorithm_model)
		executors_.emplace_back(light);
}

void logic::Light_algorithm_executor::run_step(Resolution resolution)
{
	for (auto& executor : executors_)
		executor.run_step(resolution);
}

logic::Light_algorithm_executor::Executor::Executor(
	model::Algorithm_light light)
	: light_ {std::move(light)},
	state_duration_ {light_.stop_phase}
{
	light_.lane.light = model::Light::stop;
}

void logic::Light_algorithm_executor::Executor::run_step(
	logic::Light_algorithm_executor::Resolution resolution)
{
	switch (state_)
	{
		case State::initial:
			state_duration_ -= resolution;
			if (state_duration_ <= state_duration_.zero())
			{
				state_ = State::running;
				light_.lane.light = model::Light::prepare_to_move;
				state_duration_ = light_.prepare_to_move_duration;
			}
			break;

		case State::running:
			handle_running_state(resolution);
			break;

		#ifndef __clang__
		default:
			throw std::logic_error {"Impossible case hit"};
		#endif
	}
}

void logic::Light_algorithm_executor::Executor::handle_running_state(
	Resolution resolution)
{
	state_duration_ -= resolution;
	if (state_duration_ <= state_duration_.zero())
	{
		switch (light_.lane.light)
		{
			case model::Light::move:
				state_duration_ = light_.prepare_to_stop_duration;
				light_.lane.light = model::Light::prepare_to_stop;
				break;

			case model::Light::prepare_to_move:
				state_duration_ = light_.move_duration;
				light_.lane.light = model::Light::move;
				break;

			case model::Light::prepare_to_stop:
				state_duration_ = light_.stop_duration;
				light_.lane.light = model::Light::stop;
				break;

			case model::Light::stop:
				state_duration_ = light_.prepare_to_move_duration;
				light_.lane.light = model::Light::prepare_to_move;
				break;

			case model::Light::no_light:
				throw std::invalid_argument {"Error, the algorithm cannot run "
					"for a lane without traffic lights; lane id: "
					+ light_.lane.id.value};

			#ifndef __clang__
			default:
				throw std::logic_error {"Impossible case"};
			#endif
		}
	}
}
