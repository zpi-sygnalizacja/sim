#pragma once

// model
#include <model/Lane.hpp>

// std
#include <iosfwd>
#include <unordered_map>
#include <vector>

namespace logic
{
	class Traffic_participant
	{
	public:
		enum class State
		{
			created,
			waiting,
			moving,
			delayed_moving,
			blocked,
			left_intersection
		};

		using Resolution = std::chrono::milliseconds;
		using Id = unsigned long;

		using Lane_traffic = std::vector<Traffic_participant>;
		using Traffic = std::unordered_map<model::Lane_id, Lane_traffic>;
		using Conflicting_lanes
			= std::vector<std::tuple<
				std::reference_wrapper<const model::Lane>,
				std::reference_wrapper<const model::Destination>,
				bool>>;

		using Type = model::Lane_type;

		Traffic_participant(
			const model::Lane& source,
			const model::Lane& destination,
			model::Lane_type type,
			const Conflicting_lanes& conflicting_lanes,
			Resolution move_duration,
			Resolution delay_duration);

		void make_step(
			Resolution resolution,
			const Traffic& current_traffic);

		const model::Lane& get_source() const;
		const model::Lane& get_destination() const;
		State get_state() const;
		Id get_id() const;
		Type get_type() const;

	private:
		static Id next_id();

		void make_pedestrian_step(Resolution resolution);
		void make_vehicle_step(
			Resolution resolution,
			const Traffic& current_traffic);

		const Traffic_participant* pre(const Traffic& traffic);
		bool conflict_at_crossing_crosswalk(const Traffic& traffic);
		bool conflict(const Traffic& traffic);

		std::reference_wrapper<const model::Lane> source_;
		std::reference_wrapper<const model::Lane> destination_;
		std::reference_wrapper<const Conflicting_lanes> conflicting_lanes_;

		model::Lane_type type_;
		State state_ {State::created};
		Resolution delay_duration_;
		Resolution initial_delay_duration_;
		Resolution move_duration_left_;
		Id id_;
	};

	std::ostream& operator <<(std::ostream& os, Traffic_participant::State s);
}
