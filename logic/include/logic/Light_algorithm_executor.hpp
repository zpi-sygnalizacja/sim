#pragma once

// model
#include <model/Algorithm.hpp>

// std
#include <chrono>

namespace logic
{
	class Light_algorithm_executor
	{
	public:
		using Resolution = std::chrono::milliseconds;

		Light_algorithm_executor(
			const model::Algorithm& algorithm_model);

		void run_step(Resolution resolution);

	private:
		class Executor
		{
		public:
			Executor(model::Algorithm_light light);

			void run_step(Resolution resolution);
		private:
			enum class State
			{
				initial,
				running
			};

			void handle_running_state(Resolution resolution);

			State state_ {State::initial};
			model::Algorithm_light light_;
			Resolution state_duration_;
		};

		std::vector<Executor> executors_;
	};
}
