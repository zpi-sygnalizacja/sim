#pragma once

// logic
#include <logic/Traffic_participant.hpp>


namespace logic
{
	using Lane_traffic = Traffic_participant::Lane_traffic;
	using Traffic = Traffic_participant::Traffic;
}
