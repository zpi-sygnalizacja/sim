#pragma once

// logic
#include <logic/Traffic.hpp>

// model
#include <model/Algorithm.hpp>

// std
#include <chrono>
#include <iosfwd>
#include <random>

namespace model
{
	class Intersection;
	class Segment;
}

namespace logic
{
	class Stream_generator
	{
	public:
		Stream_generator(
			const model::Intersection& intersection,
			Traffic& traffic);
		using Resolution = std::chrono::milliseconds;

		void run_step(Resolution resolution);

	private:
		class Lane_stream_generator
		{
		public:
			Lane_stream_generator(
				const::model::Intersection& intersection,
				const model::Lane& lane,
				Traffic& traffic);

			void run_step(Resolution resolution);

			enum class Turn
			{
				right  = -1,
				straight = 2,
				left = 1,
				u_turn = 0
			};

		private:
			enum class State
			{
				initial_phase,
				in_wave,
				outside_wave
			};

			using Conflicting_lanes = std::unordered_map<model::Lane_id,
				Traffic_participant::Conflicting_lanes>;

			static const model::Segment* get_segment(
				const model::Intersection& intersection,
				const model::Lane& lane,
				Turn turn);

			static Turn determine_turn_type(
				const::model::Intersection& intersection,
				const model::Lane& lane,
				const model::Destination& destination);

			void generate_wave();
			void generate_participant();
			void generate_conflicting_lanes(
				const model::Intersection& intersection);
			Turn determine_turn_type(
				const::model::Intersection& intersection,
				const model::Destination& destination);

			Turn determine_turn_type(
				const::model::Intersection& intersection,
				model::Lane_id lane_id);

			const model::Lane& choose_destination_lane();

			void find_crossing_crosswalks(
				const model::Intersection& intersection,
				const model::Destination& destination);

			void find_crossed_lanes(
				const model::Intersection& intersection,
				const model::Destination& destination);

			void find_conflicting_turns(
				const model::Intersection& intersection,
				const model::Destination& destination,
				Turn turn);

			Conflicting_lanes conflicting_lanes_;
			const model::Lane& lane_;
			Traffic& traffic_;
			Resolution state_duration_;
			std::bernoulli_distribution size_distribution_;
			std::discrete_distribution<std::size_t> destination_distribution_;
			State state_ {State::initial_phase};
		};

		std::vector<Lane_stream_generator> lane_stream_generators_;

	public:
		using Turn_type = Lane_stream_generator::Turn;
	};

	std::ostream& operator <<(std::ostream& os, Stream_generator::Turn_type t);
}
