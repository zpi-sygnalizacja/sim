#pragma once

// logic
#include <logic/Light_algorithm_executor.hpp>
#include <logic/Stream_generator.hpp>
#include <logic/Traffic.hpp>

// model
#include <model/Intersection.hpp>

// std
#include <chrono>
#include <iostream>

namespace model
{
	class Intersection;
}

namespace logic
{
	class Simulator
	{
	public:
		using Resolution = std::chrono::milliseconds;

		Simulator(
			const model::Intersection& intersection,
			Light_algorithm_executor& algorithm);

		template <class Callable>
		void run(
			std::chrono::minutes simulation_duration,
			Callable callback,
			std::ostream& os);

		static Resolution resolution();

		const Traffic& get_traffic();

	private:
		void make_traffic_step();
		void handle_chained_crosswalks();
		void remove_old_traffic();

		const model::Intersection& intersection_;
		Light_algorithm_executor& algorithm_;
		Traffic current_traffic_;
		Stream_generator stream_generator_;
	};

	template <class Callable>
	void Simulator::run(
		std::chrono::minutes simulation_duration,
		Callable callback,
		std::ostream& os
	)
	{
		for (Resolution t {}; t < simulation_duration; t += resolution())
		{
			algorithm_.run_step(resolution());
			make_traffic_step();
			stream_generator_.run_step(resolution());
			handle_chained_crosswalks();
			callback(t, intersection_, current_traffic_, os);
			// support chained crosswalks
			remove_old_traffic();
		}
	}

	void debug_callback(std::chrono::milliseconds t,
		const model::Intersection& intersection,
		const Traffic& traffic,
		std::ostream& os);
}
