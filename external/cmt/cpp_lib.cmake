# This file is part of cmt.
# Copyright (C) 2016  Maciej Piotr Sauermann
#
# cmt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Options:
#	doc		Enables to optionally generate Doxygen HTML documentation
#	tests	Enables to optionally compile tests from the tests subdirectory
#	sample	Enables to optionally compile a sample from the sample subdirectory

include(${CMAKE_CURRENT_LIST_DIR}/cpp.cmake)

if(${doc})
	option(${PROJECT_NAME}_doc "Build the documentation for the library" OFF)

	if(${PROJECT_NAME}_doc)
		find_package(Doxygen REQUIRED)
		configure_file(
			${CMAKE_CURRENT_SOURCE_DIR}/doc/Doxyfile 
			${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile @ONLY)
		add_custom_target(${PROJECT_NAME}_doc ALL
			COMMAND
				${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/doc/Doxyfile
			COMMENT "Doxygen HTML documentation")
	endif(${PROJECT_NAME}_doc)
endif(${doc})

if(${tests})
	option(${PROJECT_NAME}_tests "Build the tests for the project" OFF)

	if(${PROJECT_NAME}_tests)
		add_subdirectory(tests)
	endif(${PROJECT_NAME}_tests)
endif(${tests})

if(${sample})
	option(${PROJECT_NAME}_sample "Build the sample for the project" OFF)

	if(${PROJECT_NAME}_sample)
		add_subdirectory(sample)
	endif(${PROJECT_NAME}_sample)
endif(${sample})
