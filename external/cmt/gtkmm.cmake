# This file is part of cmt.
# Copyright (C) 2016  Maciej Piotr Sauermann
#
# cmt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Options:
#	libcpp	Enables to optionally use libc++ as a required library
#	name	Target name
#	std		C++ standard version (e.g. 11, 14, 1z)
#	lib		External libraries which are to be linked to
#	src		Source list; "file.hpp" uses the file from "include/${name}"
#			directory; "file.hxx" and "file.cpp" use the file from "src"
#			 directory

include(${CMAKE_CURRENT_LIST_DIR}/cpp.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/pthread.cmake)

## CMake dependencies ##
find_package(PkgConfig REQUIRED)

## External dependencies ##
#  gtkmm
pkg_check_modules(GTKMM REQUIRED gtkmm-3.0)
#link_directories(${GTKMM_LIBRARY_DIRS}) # this option probably is not required

# Target properties setting
macro(cmt_set_gtkmm_target_properties)

	cmt_set_target_properties()
	target_include_directories(${name}
		SYSTEM PUBLIC ${GTKMM_INCLUDE_DIRS})
	target_compile_options(${name} PUBLIC ${GTKMM_CFLAGS_OTHER})
	target_link_libraries(${name} PUBLIC ${GTKMM_LIBRARIES} Threads::Threads)

endmacro(cmt_set_gtkmm_target_properties)

# Executable adding
function(cmt_add_gtkmm_executable)

	cmt_generate_src()
	add_executable(${name} ${cmt_src})
	cmt_set_gtkmm_target_properties()

	add_custom_command(TARGET ${name} PRE_BUILD
		COMMAND ${CMAKE_COMMAND} -E remove_directory
		${CMAKE_CURRENT_BINARY_DIR}/res
		COMMAND ${CMAKE_COMMAND} -E copy_directory
		${CMAKE_CURRENT_SOURCE_DIR}/res ${CMAKE_CURRENT_BINARY_DIR}/res)

endfunction(cmt_add_gtkmm_executable)

# Library adding
function(cmt_add_gtkmm_library)

	cmt_generate_src()
	add_library(${name} STATIC ${cmt_src})
	cmt_set_gtkmm_target_properties()

endfunction(cmt_add_gtkmm_library)

