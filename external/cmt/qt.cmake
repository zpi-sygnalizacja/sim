# This file is part of cmt.
# Copyright (C) 2016  Maciej Piotr Sauermann
#
# cmt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Options:
#	libcpp	Enables to optionally use libc++ as a required library
#	name	Target name
#	std		C++ standard version (e.g. 11, 14, 1z)
#	lib		External libraries which are to be linked to
#	src		Source list; "file.hpp" uses the file from "include/${name}"
#			directory; "file.hxx" and "file.cpp" use the file from "src"
#			 directory
#	ui		ui files list; "file" uses the "file" and "file.ui" from the "ui"
#			directory
#	res		res files list; "file" uses the "file" from the "res" directory
#	warn	set to "none" to disable all warnings for a specific target

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)
find_package(Qt5Widgets)

include(${CMAKE_CURRENT_LIST_DIR}/cpp.cmake)

# Source list generation
macro(cmt_generate_qt_src)

	cmt_generate_src()

	foreach(cmt_file IN LISTS ui)
		list(APPEND cmt_src ui/${cmt_file})
		list(APPEND cmt_src ui/${cmt_file}.ui)
	endforeach()

	foreach(cmt_file IN LISTS res)
		list(APPEND cmt_src res/${cmt_file})
	endforeach()

endmacro(cmt_generate_qt_src)

# Target properties setting
macro(cmt_set_qt_target_properties)

	cmt_set_target_properties()
	target_include_directories(${name}
		PUBLIC ui PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
	target_compile_options(${name} PRIVATE
		-Wno-exit-time-destructors -Wno-global-constructors
		-Wno-undefined-reinterpret-cast)
	target_link_libraries(${name} PUBLIC Qt5::Widgets)

endmacro(cmt_set_qt_target_properties)

# Executable adding
function(cmt_add_qt_executable)

	cmt_generate_qt_src()
	add_executable(${name} ${cmt_src})
	cmt_set_qt_target_properties()

endfunction(cmt_add_qt_executable)

# Library adding
function(cmt_add_qt_library)

	cmt_generate_qt_src()
	add_library(${name} STATIC ${cmt_src})
	cmt_set_qt_target_properties()

endfunction(cmt_add_qt_library)

