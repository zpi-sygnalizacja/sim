# This file is part of cmt.
# Copyright (C) 2016  Maciej Piotr Sauermann
#
# cmt is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cmt is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Options:
#	libcpp	Enables to optionally use libc++ as a required library
#	name	Target name
#	std		C++ standard version (e.g. 11, 14, 1z)
#	lib		External libraries which are to be linked to
#	src		Source list; file.hpp uses the file from include/${name} directory;
#			file.hxx and file.cpp use the file from src directory
#	warn	set to "none" to disable all warnings for a specific target

# libc++ option
if(${libcpp})

	option(${PROJECT_NAME}_libcpp "Allow to use libc++ with Clang" ON)

	if(${PROJECT_NAME}_libcpp)
		if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
			set(cmt_stdlib_flags -stdlib=libc++)
			set(cmt_stdlib c++ c++abi)
#		else()
#			message(FATAL_ERROR "Clang and libc++ are required")
		endif()
	endif()

endif(${libcpp})

# Warnings & flags
set(cmt_warnings -Werror -pedantic -pedantic-errors)
set(cmt_flags -march=native)

if(NOT "${sanitize}" STREQUAL "")
	set(cmt_flags ${cmt_flags} -fsanitize=${sanitize}
		-fno-omit-frame-pointer)
	if(NOT "${sanitize_blacklist}" STREQUAL "")
		set(cmt_flags ${cmt_flags} -fsanitize-blacklist=${sanitize_blacklist})
	endif()
endif()

if(${lto})
	set(cmt_flags ${cmt_flags} -flto)
endif()

if(${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
	set(cmt_warnings ${cmt_warnings} -Weverything
		-Wno-padded -Wno-weak-vtables -Wno-documentation-unknown-command
		-Wno-c++98-compat -Wno-c++98-compat-pedantic)
elseif(${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
	set(cmt_warnings ${cmt_warnings} -Wall -Wextra -Wabi -Wconversion
		-Wcast-align -Wcast-qual -Winit-self -Wctor-dtor-privacy -Wformat=2
		-Wdisabled-optimization -Wnoexcept -Wlogical-op
		-Wmissing-declarations -Wmissing-include-dirs -Wshadow
		-Wold-style-cast -Woverloaded-virtual -Wredundant-decls
		-Wswitch-default -Wsign-conversion -Wsign-promo
		-Wstrict-null-sentinel -Wundef -Wstrict-overflow=5)
else()
	message(WARNING "An unsupported compiler used")
endif()

# Source list generation
macro(cmt_generate_src)

	unset(cmt_src)

	foreach(cmt_file IN LISTS src)

		# Add a .hpp file
		string(FIND ${cmt_file} ".cpp" cmt_position)
		if(${cmt_position} EQUAL -1)
			list(APPEND cmt_src include/${name}/${cmt_file})
		endif()

		# Add a .cpp file
		string(FIND ${cmt_file} ".hpp" cmt_hpp_position)
		string(FIND ${cmt_file} ".h" cmt_h_position)
		if(${cmt_h_position} EQUAL -1 AND ${cmt_hpp_position} EQUAL -1)
			list(APPEND cmt_src src/${cmt_file})
		endif()

	endforeach()

endmacro(cmt_generate_src)

# Target properties setting
macro(cmt_set_target_properties)

	target_include_directories(${name} PUBLIC include)
	target_compile_options(${name} PUBLIC ${cmt_flags} ${cmt_stdlib_flags})
	target_compile_options(${name} PRIVATE -std=c++${std})

	if(${lto})
		set_target_properties(${name} PROPERTIES LINK_FLAGS -flto)
	endif()

	if (NOT "${sanitize}" STREQUAL "")
		set_target_properties(${name} PROPERTIES LINK_FLAGS
			-fsanitize=${sanitize})
	endif()

	if (NOT "${warn}" STREQUAL none)
		target_compile_options(${name} PRIVATE ${cmt_warnings})
	endif()

	target_link_libraries(${name} PUBLIC ${cmt_stdlib} ${lib})

endmacro(cmt_set_target_properties)

# Executable adding
function(cmt_add_executable)

	cmt_generate_src()
	add_executable(${name} ${cmt_src})
	cmt_set_target_properties()

endfunction(cmt_add_executable)

# Library adding
function(cmt_add_library)

	cmt_generate_src()
	add_library(${name} STATIC ${cmt_src})
	cmt_set_target_properties()

endfunction(cmt_add_library)

