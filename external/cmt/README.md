# cmt
## Description

*cmt* is a set of CMake templates to ease the creation and maintenance of C++
projects. It automatically sets proper compiler warnings, enables to add various
options and avoid repetition when adding source files.

> Currently it supports:

> - GCC and Clang
> - option: libc++ as a tunable requirement
> - plain C++11/14/1z projects (executables and static libraries)
> - Qt projects
> - automatically includes **include** directory in the build process as a 
	*PUBLIC* directory
> - automatically add proper source files depending on their name

## Tutorial

### Basic C++ project

First, create the top CMakeLists.txt in the project root directory, e.g.
> \# *Licence information*

> cmake_minimum_required(VERSION 3.4)

> project(*project_name* VERSION *major*.*minor*.*revision*)

> include(external/cmt/cpp.cmake)

> \# Project-wide in-tree external dependencies

> add_subdirectory(external/*lib_1*)

> add_subdirectory(external/*lib_2*)

> \# Sub-projects

> add_subdirectory(*sub-project-1*)

> add_subdirectory(*sub-project-2*)

Next, create directories for all the sub-projects and put a CMakeLists.txt in 
each one, e.g.:
> set(name *target_name*)

> set(std *standard_version*)

> set(lib *all_external_libs*)

> set(src *source_files*)

> cmt_add_library() *or* cmt_add_executable()

where:

- *target_name* is the executable or library name (use **${PROJECT_NAME}** to 
	get the project's name),
- *standard_version* is **11**, **14** or **1z**,
- *all_external_libs* lists all external libraries (the exact list depends on 
	used libraries),
- *source_files* is a list of sources; a **file** means both **src/file** and 
	**include/*name*/file**, where **file.cpp** means only the former and 
	**file.h(pp)** - the latter.

Write `set(warn none)` before `cmt_add_library()`  or `cmt_add_executable()` 
calls to suppress all warnings for a given target.

### C++ static library

When including `cpp_lib.cmake`, additional variables can be set **before** the
inclusion:

- *doc* adds a Doxygen documentation option,
- *tests* adds a tests option; the tests must be placed in the *tests* 
	subdirectory,
- *sample* adds a sample options; the sample must be placed in the  *sample* 
	subdirectory

e.g.
> set(tests YES)

> include(external/cmt/cpp_lib.cmake)

### Qt project
The **qt.cmake** turns on several Qt precompilers and calls **find** for the 
Qt 5 widgets library.

Moreover, it uses two more variables to generate sources list:

- **ui** lists all .ui files and their .h(pp) and .cpp implementations - they 
	must be placed in the **ui** subdirectory,
- **res** lists all resources files - they must be placed in the **res** 
	subdirectory.

Finally, the template disables some warnings due to Qt precompiler generating
code that would trigger the *cmt* templates default warnings.
