// This file is part of util.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// util is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// util is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UTIL_TIMED_INVOKE_HPP
#define UTIL_TIMED_INVOKE_HPP

// std
#include <chrono>
#include <tuple>
#include <type_traits>
#include <utility>

namespace util
{
	namespace detail
	{
		template <
			class Callable,
			class... Args
		>
		auto timed_invoke(Callable&& callable, Args&&... args, std::false_type)
		{
			using clock = std::chrono::steady_clock;
			auto start = clock::now();

			auto value = std::forward<Callable>(callable)(
				std::forward<Args>(args)...);

			return std::make_pair(value, clock::now() - start);
		}

		/// Invoke the @p callable with @p args and return how long it took
		template <
			class Callable,
			class... Args
		>
		auto timed_invoke(Callable&& callable, Args&&... args, std::true_type)
		{
			using clock = std::chrono::steady_clock;
			auto start = clock::now();

			std::forward<Callable>(callable)(std::forward<Args>(args)...);

			return clock::now() - start;
		}
	}

	/// Invoke the @p callable with @p args and return how long it took
	template <
		class Callable,
		class... Args
	>
	auto timed_invoke(Callable&& callable, Args&&... args)
	{
		return detail::timed_invoke(
			std::forward<Callable>(callable),
			std::forward<Args>(args)...,
			std::is_same<void, decltype(std::forward<Callable>(callable)(
			std::forward<Args>(args)...))> {});
	}
}

#endif
