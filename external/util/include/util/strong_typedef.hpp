// This file is part of util.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// util is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// util is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef UTIL_STRONG_TYPEDEF_HPP
#define UTIL_STRONG_TYPEDEF_HPP

// std
#include <iosfwd>
#include <functional>
#include <type_traits>
#include <utility>

namespace util
{
	template <class ValueType, class Tag>
	class strong_typedef
	{
		template <class T>
		static constexpr auto is_constructible
			= std::is_constructible<ValueType, T>::value;

		template <class T>
		static constexpr auto is_same
			= std::is_same<ValueType, std::decay_t<T>>::value;

	public:
		using value_type = ValueType;
		using tag = Tag;
		using this_type = strong_typedef<value_type, tag>;

		value_type value;

		constexpr strong_typedef() noexcept(noexcept(value_type {}))
			: value {}
		{
		}

		explicit constexpr strong_typedef(const value_type& initial_value)
		noexcept(noexcept(value_type {initial_value}))
			: value {initial_value}
		{
		}

		explicit constexpr strong_typedef(value_type&& initial_value)
		noexcept(noexcept(value_type {std::move(initial_value)}))
			: value {std::move(initial_value)}
		{
		}

		explicit operator const value_type& () const noexcept
		{
			return value;
		}

		template <class To>
		auto to() const noexcept(noexcept(static_cast<To>(value)))
			-> decltype(static_cast<To>(value))
		{
			return static_cast<To>(value);
		}

		template <
			class Value, class = std::enable_if_t<is_constructible<Value>>>
		static auto from(Value&& value)
		{
			return this_type {
				static_cast<value_type>(std::forward<Value>(value))};
		}

		static auto default_value()
		{
			return this_type {};
		}
	};

	////////////////////////////////////////////////////////////////////////////
	// Comparison operators
	////////////////////////////////////////////////////////////////////////////
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfloat-equal"
#endif

	template <class V, class W, class Tag>
	auto operator ==(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value == y.value)
	{
		return x.value == y.value;
	}

	template <class V, class W, class Tag>
	auto operator !=(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value != y.value)
	{
		return x.value != y.value;
	}

#ifdef __clang
#pragma clang pop
#endif

	template <class V, class W, class Tag>
	auto operator <(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value < y.value)
	{
		return x.value < y.value;
	}

	template <class V, class W, class Tag>
	auto operator <=(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value <= y.value)
	{
		return x.value <= y.value;
	}

	template <class V, class W, class Tag>
	auto operator >(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value > y.value)
	{
		return x.value > y.value;
	}

	template <class V, class W, class Tag>
	auto operator >=(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value >= y.value)
	{
		return x.value >= y.value;
	}

	////////////////////////////////////////////////////////////////////////////
	// Mathematical operators
	////////////////////////////////////////////////////////////////////////////
	template <class V, class W, class Tag>
	auto operator +(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value + y.value), Tag> {
						 x.value + y.value})
	{
		return strong_typedef<decltype (x.value + y.value), Tag> {
			x.value + y.value};
	}

	template <class V, class W, class Tag>
	auto operator -(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value - y.value), Tag> {
						 x.value - y.value})
	{
		return strong_typedef<decltype (x.value - y.value), Tag> {
			x.value - y.value};
	}

	template <class V, class W, class Tag>
	auto operator *(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value * y.value), Tag> {
						 x.value * y.value})
	{
		return strong_typedef<decltype (x.value * y.value), Tag> {
			x.value * y.value};
	}

	template <class V, class W, class Tag>
	auto operator /(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value / y.value), Tag> {
						 x.value / y.value})
	{
		return strong_typedef<decltype (x.value / y.value), Tag> {
			x.value / y.value};
	}

	template <class V, class W, class Tag>
	auto operator %(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value % y.value), Tag> {
						 x.value % y.value})
	{
		return strong_typedef<decltype (x.value % y.value), Tag> {
			x.value % y.value};
	}

	template <class V, class W, class Tag>
	auto operator +=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value += y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value += y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator -=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value -= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value -= y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator *=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value *= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value *= y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator /=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value /= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value /= y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator %=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value %= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value %= y.value;
		return x;
	}

	////////////////////////////////////////////////////////////////////////////
	// Logical operators
	////////////////////////////////////////////////////////////////////////////
	template <class ValueType, class Tag>
	auto operator !(
		const strong_typedef<ValueType, Tag>& x)
		-> decltype (!x.value)
	{
		return !x.value;
	}

	////////////////////////////////////////////////////////////////////////////
	// Increment & decrement operators
	////////////////////////////////////////////////////////////////////////////
	template <class ValueType, class Tag>
	auto operator ++(
		strong_typedef<ValueType, Tag>& x)
		-> decltype (++x.value,
			std::declval<strong_typedef<ValueType, Tag>>()) &
	{
		++x.value;
		return x;
	}

	template <class ValueType, class Tag>
	auto operator --(
		strong_typedef<ValueType, Tag>& x)
		-> decltype (--x.value,
			std::declval<strong_typedef<ValueType, Tag>>()) &
	{
		--x.value;
		return x;
	}

	template <class ValueType, class Tag>
	auto operator ++(
		strong_typedef<ValueType, Tag>& x, int)
		-> decltype (++x.value, strong_typedef<ValueType, Tag> {x})
	{
		auto copy = x;
		++x.value;
		return copy;
	}

	template <class ValueType, class Tag>
	auto operator --(
		strong_typedef<ValueType, Tag>& x, int)
		-> decltype (--x.value, strong_typedef<ValueType, Tag> {x})
	{
		auto copy = x;
		--x.value;
		return copy;
	}

	////////////////////////////////////////////////////////////////////////////
	// Binary bit operators
	////////////////////////////////////////////////////////////////////////////
	template <class V, class W, class Tag>
	auto operator ^(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value ^ y.value), Tag> {
						 x.value ^ y.value})
	{
		return strong_typedef<decltype (x.value ^ y.value), Tag> {
			x.value ^ y.value};
	}

	template <class V, class W, class Tag>
	auto operator ^=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value ^= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value ^= y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator &(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value & y.value), Tag> {
						 x.value & y.value})
	{
		return strong_typedef<decltype (x.value & y.value), Tag> {
			x.value & y.value};
	}

	template <class V, class W, class Tag>
	auto operator &=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value &= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value &= y.value;
		return x;
	}

	template <class V, class W, class Tag>
	auto operator |(
		const strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (strong_typedef<decltype (x.value | y.value), Tag> {
						 x.value | y.value})
	{
		return strong_typedef<decltype (x.value | y.value), Tag> {
			x.value | y.value};
	}

	template <class V, class W, class Tag>
	auto operator |=(
		strong_typedef<V, Tag>& x,
		const strong_typedef<W, Tag>& y)
		-> decltype (x.value |= y.value,
			std::declval<strong_typedef<V, Tag>>()) &
	{
		x.value |= y.value;
		return x;
	}

	template <class ValueType, class Tag>
	auto operator ~(
		const strong_typedef<ValueType, Tag>& x)
		-> decltype (strong_typedef<ValueType, Tag> {~x.value})
	{
		return strong_typedef<ValueType, Tag> {~x.value};
	}


	////////////////////////////////////////////////////////////////////////////
	// stream operators
	////////////////////////////////////////////////////////////////////////////
	template <class ValueType, class Tag>
	auto operator <<(
		std::ostream& os,
		const strong_typedef<ValueType, Tag>& x)
		-> decltype (os << x.value)
	{
		return os << x.value;
	}

	template <class ValueType, class Tag>
	auto operator >>(
		std::istream& is,
		strong_typedef<ValueType, Tag>& x)
		-> decltype (is >> x.value)
	{
		return is >> x.value;
	}

	////////////////////////////////////////////////////////////////////////////
	// Next & previous & zero functions
	////////////////////////////////////////////////////////////////////////////
	template <class ValueType, class Tag>
	auto next(const strong_typedef<ValueType, Tag>& x)
	{
		auto y = x;
		return ++y;
	}

	template <class ValueType, class Tag>
	auto prev(const strong_typedef<ValueType, Tag>& x)
	{
		auto y = x;
		return --y;
	}

	template <class ValueType, class Tag>
	auto default_value(const strong_typedef<ValueType, Tag>& x)
	{
		return x.default_value();
	}

	template <class ValueType, class Tag>
	auto is_default_value(const strong_typedef<ValueType, Tag>& x)
	{
		return x == default_value(x);
	}

	template <class ValueType, class Tag>
	auto is_not_default_value(const strong_typedef<ValueType, Tag>& x)
	{
		return x != default_value(x);
	}
}


////////////////////////////////////////////////////////////////////////////////
// hash functor
////////////////////////////////////////////////////////////////////////////////
namespace std
{
	template <class ValueType, class Tag>
	struct hash<util::strong_typedef<ValueType, Tag>>
	{
		using argument_type = util::strong_typedef<ValueType, Tag>;
		using result_type = std::size_t;

		result_type operator()(const argument_type& x) const
		{
			return hash<ValueType> {}(x.value);
		}
	};
}

#endif
