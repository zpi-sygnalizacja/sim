// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_PRE_HPP
#define UNIT_PRE_HPP

// std
#include <type_traits>
#include <utility>

// unit
#include <unit/exception.hpp>
#include <unit/type_traits.hpp>

namespace unit
{
	/// Exception thrown by default by the pre() function.
	class pre_failed : public exception
	{
	public:
		/// Return a message containing the exception's class name
		const char* what() const noexcept override;
	};

	/// Check if @p precondition is @c true
	/// If it is not, throw an exception.
	/// @tparam ExceptionType The type of exception that can be thrown
	/// @param precondition A precondition that must be @c true
	/// @param exception_args Arguments passed to the exception's constructor
	/// @throw ExceptionType Thrown if @p precondition is @c false
	template <
		class ExceptionType = pre_failed,
		class Bool,
		class... ExceptionArgs,
		class = std::enable_if_t<detail::is_convertible_to_bool<Bool&&>::value
			&& std::is_constructible<ExceptionType, ExceptionArgs&&...>::value>>
	void pre(Bool&& precondition, ExceptionArgs&&... exception_args)
	{
		if (!std::forward<Bool>(precondition))
			throw ExceptionType {
				std::forward<ExceptionArgs>(exception_args)...};
	}
}

#endif
