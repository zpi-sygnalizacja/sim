// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_OSTREAM_FORMATTER_HPP
#define UNIT_OSTREAM_FORMATTER_HPP

// std
#include <iosfwd>
#include <string>

// unit
#include <unit/formatter.hpp>

namespace unit
{
	class ostream_formatter : public formatter
	{
	public:
		ostream_formatter(std::ostream& os, int line_width = 80);

		void suite_started(const std::string& suite_name,
			size_type suite_size) override;

		void test_finished(const std::string& test_name,
			const test_result& result) override;

		void suite_finished(size_type passed, size_type failed) override;

	private:
		void print_title_line();
		void print_header_line();
		void print_summary_line();

		std::ostream& fill_line(char separator);

		std::ostream& os_;
		size_type test_no_;

		const int line_width_;
		int test_no_width_;
		int name_width_;

		const int duration_width_ = 5;
		const int result_width_ = 4;
		const int spaces_count_ = 3;

		static const char line_separator_ = '-';
		static const char header_separator_ = '#';
	};
}

#endif
