// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_SUITE_HPP
#define UNIT_SUITE_HPP

// std
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

// unit
#include <unit/exception.hpp>
#include <unit/test.hpp>

namespace unit
{
	/// Exception thrown by the suite::at() and suite::operator[]() functions
	class test_not_found : public exception
	{
	public:
		/// Return a message containing the exception's class name
		const char* what() const noexcept override;
	};

	/// Exception thrown by the suite::push() and suite::emplace() functions
	class test_name_not_unique : public exception
	{
	public:
		/// Return a message containing the exception's class name
		const char* what() const noexcept override;
	};

	/// A test suite
	class suite final
	{
		using rep = std::vector<test>;

	public:
		template <class... Args>
		using is_test_constructible = std::is_constructible<test, Args...>;

		/// The iterator type
		using iterator = rep::iterator;

		/// The const_iterator type
		using const_iterator = rep::const_iterator;

		/// The size() type
		using size_type = rep::size_type;

		/// Construct a test suite
		explicit suite(std::string name = "") noexcept;

		// Get the suite's name
		const std::string& name() const noexcept;

		/// Get the number of tests in the suite
		size_type size() const noexcept;

		/// Get a test named @p test_name
		/// @throw test_not_found Thrown if a test named @p test_name is not
		/// found in the suite
		const test& at(const std::string& test_name) const;

		/// Get a test named @p test_name
		/// @throw test_not_found Thrown if a test named @p test_name is not
		/// found in the suite
		const test& operator[](const std::string& test_name) const;

		/// Add a new test
		/// @pre The suite does not contain a test named the same as @p test.
		/// @throw test_name_not_unique Thrown if the precondition is not met
		void push(const test& new_test);

		/// @overload
		void push(test&& new_test);

		/// Add a new test
		/// @pre The suite does not contain a test named the same as @p test.
		/// @throw test_name_not_unique Thrown if the precondition is not met
		template <class... Args,
			class = std::enable_if_t<is_test_constructible<Args&&...>::value>>
		void emplace(Args&&... args)
		{
			test new_test {std::forward<Args>(args)...};

			auto p = find(new_test.name());
			if (p != tests_.end())
				throw test_name_not_unique();

			tests_.push_back(std::move(new_test));
		}

		iterator begin() noexcept;
		iterator end() noexcept;
		const_iterator begin() const noexcept;
		const_iterator end() const noexcept;
		const_iterator cbegin() const noexcept;
		const_iterator cend() const noexcept;

	private:
		/// Get an iterator to a test named @p test_name
		const_iterator find(const std::string& test_name) const noexcept;

		std::vector<test> tests_;
		std::string name_;
	};
}

#endif
