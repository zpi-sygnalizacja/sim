// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_CHECK_NOTHROW_HPP
#define UNIT_CHECK_NOTHROW_HPP

// std
#include <exception>
#include <utility>

// unit
#include <unit/exception_thrown.hpp>

namespace unit
{
	/// Check if @p functor call does not throw
	/// If it throws an exception, throw an exception with the original
	/// exception nested.
	/// @tparam Callable A callable type, e.g. a type of a lambda functor
	/// @param functor The functor which will be called
	/// @param args Optional argument for the functor call
	/// @throw exception_thrown Thrown if @p functor call throws; the original
	/// exception is saved and can be accessed through the
	/// exception_thrown::get_exception() method
	template <class Callable, class... Args>
	void check_nothrow(Callable&& functor, Args&&... args)
	{
		try
		{
			std::forward<Callable>(functor)(std::forward<Args>(args)...);
		}
		catch (...)
		{
			throw exception_thrown {std::current_exception()};
		}
	}
}

#endif
