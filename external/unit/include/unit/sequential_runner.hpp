// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_SEQUENTIAL_RUNNER_HPP
#define UNIT_SEQUENTIAL_RUNNER_HPP

// std
#include <memory>
#include <string>

// unit
#include <unit/formatter.hpp>
#include <unit/suite.hpp>
#include <unit/suite_maker.hpp>
#include <unit/test_result.hpp>

namespace unit
{
	/// Exception thrown by the sequential_formatter constructor
	class nullptr_formatter : public exception
	{
	public:
		/// Return a message containing the exception's class name
		const char* what() const noexcept override;
	};

	/// A runner which runs a test suite sequentially
	class sequential_runner
	{
		using size_type = formatter::size_type;
	public:
		/// Construct a runner
		/// @pre @p formatter points to a valid formatter, i.e. != @c nullptr
		sequential_runner(std::unique_ptr<formatter> formatter);

		/// Get the current formatter
		const formatter& current_formatter() const noexcept;

		/// Run a suite
		/// @return @c true if all tests passed or there were no tests to run
		bool operator()(const suite& suite_to_run) const noexcept;

		/// @overload
		template <class SuiteClass>
		bool operator()(const suite_maker<SuiteClass>& suite_maker)
			const noexcept
		{
			return operator()(suite_maker.made_suite());
		}

	private:
		std::unique_ptr<formatter> current_formatter_;
	};
}

#endif
