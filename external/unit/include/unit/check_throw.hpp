// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_CHECK_THROW_HPP
#define UNIT_CHECK_THROW_HPP

// std
#include <exception>
#include <typeinfo>
#include <utility>

// unit
#include <unit/check_failed.hpp>
#include <unit/exception_thrown.hpp>

namespace unit
{
	/// Type equality enumeration
	enum class type_equality
	{
		inheritance, ///< An inherited type is considered equal to a given type
		type_info ///< Only the exact type is considered equal to a given type
	};

	/// Check if @p functor call throws an exception of type @c ExceptionType
	/// If it throws an other exception type, throw an exception with the
	/// original exception nested. Also throw an exception if the call does not
	/// throw at all.
	/// @tparam ExceptionType The expected exception type
	/// @tparam Callable A callable type, e.g. a type of a lambda functor
	/// @param required_equality If @c type_equality::type_info, also compare
	/// the thrown exception's type_info to the expected one's type_info
	/// @param functor The functor which will be called
	/// @param args Optional argument for the functor call
	/// @throw exception_thrown Thrown if @p functor call throws an invalid
	/// exception type; the original exception is saved and can be accessed
	/// through the exception_thrown::get_exception() method
	/// @throw check_failed Thrown if @p functor does not throw at all
	template <class ExceptionType, class Callable, class... Args>
	ExceptionType check_throw(Callable&& functor,
		type_equality required_equality,
		Args&&... args)
	{
		try
		{
			std::forward<Callable>(functor)(std::forward<Args>(args)...);
		}
		catch (ExceptionType& e)
		{
			auto check_type_info_equality
				= required_equality == type_equality::type_info;
			if (check_type_info_equality && typeid(e) != typeid(ExceptionType))
				throw exception_thrown {std::current_exception()};
			return e;
		}
		catch (...)
		{
			throw exception_thrown {std::current_exception()};
		}
		throw check_failed {};
	}

	/// @overload
	template <class ExceptionType, class Callable, class... Args>
	ExceptionType check_throw(Callable&& functor, Args&&... args)
	{
		return check_throw<ExceptionType>(
			std::forward<Callable>(functor), type_equality::type_info,
			std::forward<Args>(args)...);
	}
}

#endif
