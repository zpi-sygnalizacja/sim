// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_FORMATTER_HPP
#define UNIT_FORMATTER_HPP

// std
#include <string>

// unit
#include <unit/test_result.hpp>

namespace unit
{
	/// A formatter interface used by all test runners
	class formatter
	{
	public:
		using size_type = unsigned long;

		formatter() = default;
		formatter(const formatter&) = delete;
		formatter& operator=(const formatter&) = delete;

		virtual ~formatter() = default;

		/// Called when running a suite has started
		virtual void suite_started(const std::string& suite_name,
			size_type suite_size) = 0;

		/// Called when a single test has been run
		virtual void test_finished(const std::string& test_name,
			const test_result& result) = 0;

		/// Called when running a suite has finished
		virtual void suite_finished(size_type passed, size_type failed) = 0;
	};
}

#endif
