// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_EXCEPTION_THROWN_HPP
#define UNIT_EXCEPTION_THROWN_HPP

// std
#include <exception>

// unit
#include <unit/check_failed.hpp>

namespace unit
{
	/// Exception thrown by the check_nothrow() function
	class exception_thrown : public check_failed
	{
	public:
		explicit exception_thrown(std::exception_ptr nested_exception);

		/// Return a message containing the exception's class name
		const char* what() const noexcept override;

		/// Get the recorded exception
		std::exception_ptr nested_exception() const;

	private:
		std::exception_ptr nested_exception_;
	};
}

#endif
