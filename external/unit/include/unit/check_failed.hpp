// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_CHECK_FAILED_HPP
#define UNIT_CHECK_FAILED_HPP

// unit
#include <unit/exception.hpp>

namespace unit
{
	/// Exception thrown by the check() function
	class check_failed : public exception
	{
	public:
		/// Return a message containing the exception's class name
		const char* what() const noexcept override;
	};
}

#endif

