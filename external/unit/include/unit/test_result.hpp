// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_TEST_RESULT_HPP
#define UNIT_TEST_RESULT_HPP

// std
#include <chrono>
#include <exception>

namespace unit
{
	/// A single test run result
	class test_result
	{
	public:
		/// The used clock
		using clock = std::chrono::steady_clock;

		/// The test duration
		using duration = clock::duration;

		/// Construct a test_result
		explicit test_result(duration test_duration,
			std::exception_ptr exception = nullptr);

		/// Get the test's duration
		duration test_duration() const noexcept;

		/// Get the exception thrown in the test or @c nullptr
		std::exception_ptr thrown_exception() const noexcept;

		/// Check if a test passed, i.e. get_exception() would return @c nullptr
		bool passed() const noexcept;

		/// @see passed()
		explicit operator bool() const noexcept;

	private:
		duration test_duration_;
		std::exception_ptr exception_;
	};
}

#endif
