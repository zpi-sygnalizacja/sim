// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_CHECK_FALSE_HPP
#define UNIT_CHECK_FALSE_HPP

// std
#include <type_traits>
#include <utility>

// unit
#include <unit/check_failed.hpp>
#include <unit/type_traits.hpp>

namespace unit
{
	/// Check if @p condition is @c false
	/// If it is not, throw an exception.
	/// @throw check_failed Thrown if @p condition is @c true
	template <class Bool,
		class = std::enable_if_t<detail::is_convertible_to_bool<Bool&&>::value>>
	void check_false(Bool&& condition)
	{
		if (std::forward<Bool>(condition))
			throw check_failed {};
	}
}

#endif
