// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_SUITE_MAKER_HPP
#define UNIT_SUITE_MAKER_HPP

// std
#include <string>
#include <typeinfo>
#include <type_traits>
#include <utility>

// unit
#include <unit/suite.hpp>
#include <unit/test.hpp>

namespace unit
{
	namespace detail
	{
		std::string demangle(const char* name);
	}

	/// A test suite maker
	template <class SuiteClass>
	class suite_maker
	{
	public:
		/// Get the made suite
		const suite& made_suite() const noexcept
		{
			return made_suite_;
		}

	protected:
		/// Construct a test suite
		explicit suite_maker() noexcept
			: made_suite_ {detail::demangle(typeid(SuiteClass).name())}
		{
		}

		/// Add a new test
		/// @pre The suite does not contain a test named the same as @p test.
		/// @throw test_name_not_unique Thrown if the precondition is not met
		void push(const test& new_test)
		{
			made_suite_.push(new_test);
		}

		/// @overload
		void push(test&& new_test)
		{
			made_suite_.push(std::move(new_test));
		}

		/// Add a new test
		/// @pre The suite does not contain a test named the same as @p test.
		/// @throw test_name_not_unique Thrown if the precondition is not met
		template <class... Args,
			class = std::enable_if_t<
				suite::is_test_constructible<Args&&...>::value>>
		void emplace(Args&&... args)
		{
			made_suite_.emplace(std::forward<Args>(args)...);
		}

	private:
		suite made_suite_;
	};
}

#endif
