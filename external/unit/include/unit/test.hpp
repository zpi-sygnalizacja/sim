// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
#ifndef UNIT_TEST_HPP
#define UNIT_TEST_HPP

// std
#include <functional>
#include <string>
#include <type_traits>
#include <utility>

// unit
#include <unit/test_result.hpp>

namespace unit
{
	/// A single unit test
	class test
	{
	public:
		/// The type of a unit test functor
		using functor = std::function<void()>;

		template <class T>
		using is_test_functor = std::is_same<std::decay_t<T>,
			std::decay_t<functor>>;

		/// Construct a named unit test
		explicit test(std::string name, functor test_functor);

		/// @overload
		template <class Callable, class... Args,
			class = std::enable_if_t<!is_test_functor<Callable>::value>>
		explicit test(std::string name, Callable&& callable, Args&&... args)
			: test {std::move(name),
				functor {std::bind(std::forward<Callable>(callable),
					std::forward<Args>(args)...)}}
		{
		}

		/// Run the test
		/// @note The function copies the test @a functor
		test_result operator()() const noexcept;

		/// Get the name of the test
		const std::string& name() const noexcept;

	private:
		std::string name_;
		functor functor_;
	};
}

#endif
