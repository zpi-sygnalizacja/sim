// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// unit
#include "unit/test_result.hpp"

unit::test_result::test_result(
	test_result::duration test_duration, std::exception_ptr exception)
	: test_duration_ {test_duration}, exception_ {exception}
{
}

unit::test_result::duration unit::test_result::test_duration()
	const noexcept
{
	return test_duration_;
}

std::exception_ptr unit::test_result::thrown_exception() const noexcept
{
	return exception_;
}

bool unit::test_result::passed() const noexcept
{
	return exception_ == nullptr;
}

unit::test_result::operator bool() const noexcept
{
	return passed();
}
