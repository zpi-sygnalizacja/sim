// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// std
#include <algorithm>

// unit
#include "unit/pre.hpp"
#include "unit/post.hpp"
#include "unit/suite.hpp"

const char* unit::test_not_found::what() const noexcept
{
	return "unit::test_not_found: a test with such a name has not been found";
}

const char* unit::test_name_not_unique::what() const noexcept
{
	return "unit::test_name_not_unique: a test with such a name already exists";
}

unit::suite::suite(std::string name) noexcept
	: name_ {std::move(name)}
{
}

const std::string&unit::suite::name() const noexcept
{
	return name_;
}

unit::suite::size_type unit::suite::size() const noexcept
{
	return tests_.size();
}

const unit::test& unit::suite::at(const std::string& test_name) const
{
	auto p = find(test_name);
	unit::pre<test_not_found>(p != tests_.end());

	return *p;
}

const unit::test& unit::suite::operator[](const std::string& test_name) const
{
	return at(test_name);
}

void unit::suite::push(const unit::test& new_test)
{
	unit::pre<test_name_not_unique>(find(new_test.name()) == tests_.end());

	tests_.push_back(new_test);
}

void unit::suite::push(unit::test&& new_test)
{
	unit::pre<test_name_not_unique>(find(new_test.name()) == tests_.end());

	tests_.push_back(std::move(new_test));
}

unit::suite::iterator unit::suite::begin() noexcept
{
	return tests_.begin();
}

unit::suite::iterator unit::suite::end() noexcept
{
	return tests_.end();
}

unit::suite::const_iterator unit::suite::begin() const noexcept
{
	return tests_.begin();
}

unit::suite::const_iterator unit::suite::end() const noexcept
{
	return tests_.end();
}

unit::suite::const_iterator unit::suite::cbegin() const noexcept
{
	return tests_.cbegin();
}

unit::suite::const_iterator unit::suite::cend() const noexcept
{
	return tests_.cend();
}

unit::suite::const_iterator unit::suite::find(const std::string& test_name)
	const noexcept
{
	return std::find_if(tests_.begin(), tests_.end(),
		[&test_name](auto& test){return test.name() == test_name;});
}
