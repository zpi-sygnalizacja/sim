// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// unit
#include "unit/test.hpp"

unit::test::test(std::string name, functor test_functor)
	: name_ {std::move(name)}, functor_ {std::move(test_functor)}
{
}

unit::test_result unit::test::operator()() const noexcept
{
	auto functor_copy = functor_;
	using clock = test_result::clock;
	auto start = clock::now();

	try
	{
		functor_copy();
		return test_result {clock::now() - start};
	}
	catch (...)
	{
		return test_result {clock::now() - start, std::current_exception()};
	}
}

const std::string& unit::test::name() const noexcept
{
	return name_;
}
