// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// unit
#include "unit/exception_thrown.hpp"

unit::exception_thrown::exception_thrown(std::exception_ptr nested_exception)
	: nested_exception_ {nested_exception}
{
}

const char* unit::exception_thrown::what() const noexcept
{
	return "unit::exception_thrown: an unexpected exception was thrown";
}

std::exception_ptr unit::exception_thrown::nested_exception() const
{
	return nested_exception_;
}
