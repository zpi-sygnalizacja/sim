// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// std
#include <cmath>
#include <iostream>
#include <iomanip>

// unit
#include "unit/ostream_formatter.hpp"

namespace
{
	using size_type = unit::ostream_formatter::size_type;
	int number_of_digits(size_type n)
	{
		return n > 0 ? static_cast<int>(std::log10(n)) + 1 : 1;
	}
}

unit::ostream_formatter::ostream_formatter(std::ostream& os, int line_width)
	: os_ {os}, line_width_ {line_width}
{
}

void unit::ostream_formatter::suite_started(const std::string& suite_name,
	size_type suite_size)
{
	test_no_width_ = number_of_digits(suite_size) + 1;
	test_no_ = {};

	name_width_ = line_width_ - test_no_width_ - duration_width_ - result_width_
		- spaces_count_;

	print_title_line();
	os_ << ' ' << suite_name << '\n';
	print_title_line();

	os_ << std::left << std::setw(test_no_width_) << "No" << ' '
		<< std::left << std::setw(name_width_) << "Test name" << ' '
		<< std::right << std::setw(duration_width_) << "T[ms]" << ' '
		<< std::right << std::setw(result_width_) << "Res."
		<< "\n";

	print_header_line();
}

void unit::ostream_formatter::test_finished(const std::string& test_name,
	const test_result& result)
{
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(
		result.test_duration()).count();

	os_ << std::right << std::setw(test_no_width_) << ++test_no_ << ' '
		<< std::left << std::setw(name_width_) << test_name << ' '
		<< std::right << std::setw(duration_width_) << duration
		<< ' '
		<< std::setw(result_width_) << (result ? "pass" : "fail")
		<< '\n';
}

void unit::ostream_formatter::suite_finished(size_type pass, size_type fail)
{
	print_summary_line();
	os_ << "pass: " << std::right << std::setw(test_no_width_) << pass << '\n'
		<< "fail: " << std::right << std::setw(test_no_width_) << fail << '\n';
}

void unit::ostream_formatter::print_title_line()
{
	fill_line('#') << '\n';
}

void unit::ostream_formatter::print_header_line()
{
	fill_line('-') << '\n';
}

void unit::ostream_formatter::print_summary_line()
{
	fill_line('_') << '\n';
}

std::ostream& unit::ostream_formatter::fill_line(char separator)
{
	return os_ << std::setw(line_width_) << std::setfill(separator)
		<< separator << std::setfill(' ');
}
