// This file is part of unit.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// unit is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// unit is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// unit
#include "unit/pre.hpp"
#include "unit/sequential_runner.hpp"

const char* unit::nullptr_formatter::what() const noexcept
{
	return "unit::nullptr_formatter: sequential_runner's constructor was called"
		" with a nullptr argument";
}

unit::sequential_runner::sequential_runner(
	std::unique_ptr<unit::formatter> formatter)
	: current_formatter_ {std::move(formatter)}
{
	unit::pre<nullptr_formatter>(current_formatter_);
}

const unit::formatter& unit::sequential_runner::current_formatter() const
	noexcept
{
	return *current_formatter_.get();
}

bool unit::sequential_runner::operator()(const unit::suite& suite_to_run)
	const noexcept
{
	size_type passed = 0;
	size_type failed = 0;

	current_formatter_->suite_started(suite_to_run.name(), suite_to_run.size());

	for (auto& test : suite_to_run)
	{
		auto result = test();

		current_formatter_->test_finished(test.name(), result);

		if (result)
			++passed;
		else
			++failed;
	}

	current_formatter_->suite_finished(passed, failed);
	return !failed;
}

