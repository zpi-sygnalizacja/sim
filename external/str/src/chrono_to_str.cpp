// This file is part of str.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// str is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// str is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// str
#include "str/chrono_to_str.hpp"

std::string str::to_str(std::chrono::seconds duration, str::fmt format)
{
	auto count = duration.count();
	switch (format)
	{
		case fmt::min_sec:
		{
			return std::to_string(count / 60) + ':'
					+ std::string (count % 60 < 10, '0')
					+ std::to_string(count % 60);
		}

#ifndef __clang__
		default:
			throw std::exception {};
#endif
	}
}
