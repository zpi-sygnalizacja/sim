// This file is part of str.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// str is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// str is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STR_FORMAT_HPP
#define STR_FORMAT_HPP

// std
#include <array>
#include <cctype>
#include <cstddef>
#include <exception>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

// str
#include <str/to_str.hpp>

namespace str
{
	namespace detail
	{
		template <std::size_t N>
		class formatter
		{
		public:
			formatter(
				std::string& destination,
				const std::string& fmt,
				const std::array<std::string, N>& strings)
				: destination_ {destination}, ss_ {fmt}, strings_ {strings}
			{
				parse();
			}

		private:
			static constexpr auto at = '@';

			void parse()
			{
				char c;
				while (ss_ >> std::noskipws >> c)
					parse_character(c);
			}

			void parse_character(char c)
			{
				switch (c)
				{
					case at:
						parse_at_character();
						break;

					default:
						destination_ += c;
				}
			}

			void parse_at_character()
			{
				const auto eof = std::stringstream::traits_type::eof();
				auto next = ss_.eof() ? eof : ss_.peek();

				switch (next)
				{
					case at:
						ss_.get();
						destination_ += at;
						break;

					default:
						if (std::isdigit(next))
							ss_ >> std::noskipws >> last_index_;
						destination_ += strings_.at(last_index_++);
				}
			}

			std::string& destination_;
			std::stringstream ss_;
			const std::array<std::string, N>& strings_;

			std::size_t last_index_ {};
		};
	}

	template <class... Args>
	void format_to(
		std::string& destination,
		const std::string& fmt,
		Args&&... args)
	{
		constexpr auto argc = sizeof...(args);
		std::array<std::string, argc> strings {{to_str(args)...}};

		detail::formatter<argc> formatter {destination, fmt, strings};
	}

	template <class... Args>
	std::string format(const std::string& fmt, Args&&... args)
	{
		std::string s;
		format_to(s, fmt, std::forward<Args>(args)...);
		return s;
	}
}

#endif
