// This file is part of str.
// Copyright (C) 2016  Maciej Piotr Sauermann
//
// str is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// str is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef STR_TO_STR_HPP
#define STR_TO_STR_HPP

// std
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <utility>

namespace str
{
	namespace detail
	{
		template<class T>
		constexpr auto is_std_string
			= std::is_same<std::string, std::decay_t<T>>::value;

		template <class T>
		constexpr auto has_std_to_string_impl(T*)
			-> decltype(std::to_string(std::declval<T>()), std::true_type {})
		{
			return std::true_type {};
		}

		template <class T>
		constexpr auto has_std_to_string_impl(...) -> std::false_type
		{
			return std::false_type {};
		}

		template <class T>
		constexpr auto has_std_to_string
			= has_std_to_string_impl<std::decay_t<T>>(nullptr);
	}

	template <class String,
		typename = std::enable_if_t<detail::is_std_string<String>>>
	auto to_str(String&& s) -> decltype(auto)
	{
		return std::forward<String>(s);
	}

	template <class BuiltIn,
		typename = std::enable_if_t<!detail::is_std_string<BuiltIn>>>
	auto to_str(BuiltIn x) -> decltype (std::to_string(x))
	{
		return std::to_string(x);
	}

	class conversion_failed : std::exception
	{
	public:
		using exception::exception;
	};

	template <
		class T,
		typename = std::enable_if_t<
			!detail::has_std_to_string<T> && !detail::is_std_string<T>
	>>
	auto to_str(T&& t) -> decltype((std::stringstream {} << t, std::string {}))
	{
		static_assert(!detail::has_std_to_string<T>(),
			"invalid overload chosen");

		std::stringstream ss;

		ss << std::forward<T>(t);
		if (!ss)
			throw conversion_failed {};

		return ss.str();
	}
}

#endif
